package com.evilknight.spiel.spielSchirm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evilknight.spiel.spielSchirm.objekte.FigurSpieler;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.VerfolgungsKamera;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class SpielSchirm extends ScreenAdapter{

    SpriteBatch spriteBatch;
    private VerfolgungsKamera verfolgungsKamera;


    private Level level;


    @Override
    public void show() {
        AssetManager assetManager = new AssetManager();
        Assets.instance.init(assetManager);

        level = new Level(this);

        spriteBatch = new SpriteBatch();
        verfolgungsKamera = new VerfolgungsKamera(level);
        verfolgungsKamera.kamera = level.viewport.getCamera();
        /*verfolgungsKamera.kamera = new OrthographicCamera(level.viewport.getCamera().viewportWidth, level.viewport.getCamera().viewportHeight);
        //verfolgungsKamera.kamera.translate(level.figurSpieler.getPositionMitte());*/
        verfolgungsKamera.ziel = level.getFigurSpieler();
        /*
        onscreenControls = new OnscreenControls();  TODO Bildschirmtasten nur auf handy anzeigen lassen udn Inputprocessor vergeben
        if (onMobile()) {
            Gdx.input.setInputProcessor(onscreenControls);
        } */
    }
    /*
    private boolean onMobile() {
        return Gdx.app.getType() == Application.ApplicationType.Android || Gdx.app.getType() == Application.ApplicationType.iOS;
    } */

    /*public void verfolgungskameraAusrichten () { //Wird in Level nach dem Tod des Spielers verwendet um die Kamera wieder richtig zu po
        verfolgungsKamera.kamera = level.viewport.getCamera();
        verfolgungsKamera.ziel = level.getFigurSpieler();
    } */

    @Override
    public void render(float delta) {
        level.update(delta);
        verfolgungsKamera.update(delta);

        Gdx.gl.glClearColor(Konstanten.HINTERGRUND_FARBE.r, Konstanten.HINTERGRUND_FARBE.g, Konstanten.HINTERGRUND_FARBE.b, Konstanten.HINTERGRUND_FARBE.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        level.render(spriteBatch);
    }

    @Override
    public void resize(int width, int height) {
        level.viewport.update(width, height, true);
        level.eigenerInputAdapter.overlayViewport.update(width, height, true);
        verfolgungsKamera.kamera = level.viewport.getCamera();
        //verfolgungsKamera.kamera = new OrthographicCamera(width, height);

        //onscreenControls.overlayViewport.update(width, height, true);
        //onscreenControls.recalculateButtonPositions();
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public VerfolgungsKamera getVerfolgungsKamera() {
        return verfolgungsKamera;
    }

    public void setVerfolgungsKamera(VerfolgungsKamera verfolgungsKamera) {
        this.verfolgungsKamera = verfolgungsKamera;
    }
}
