package com.evilknight.spiel.spielSchirm.overlays;

import com.badlogic.gdx.InputAdapter;

/**
 * Created by LeonPB on 03.11.2017.
 */

public class BildschirmTasten extends InputAdapter {
/*
    public final Viewport overlayViewport;
    public FigurSpieler figurSpieler;
    private Vector2 moveLeftCenter;
    private Vector2 moveRightCenter;
    private Vector2 shootCenter;
    private Vector2 jumpCenter;
    private int moveLeftPointer;
    private int moveRightPointer;
    private int jumpPointer;

    public BildschirmTasten() {
        this.overlayViewport = new ExtendViewport(Konstanten.BILDSCHIRMTASTEN_VIEWPORT_GROESSE, Konstanten.BILDSCHIRMTASTEN_VIEWPORT_GROESSE);

        moveLeftCenter = new Vector2();
        moveRightCenter = new Vector2();
        shootCenter = new Vector2();
        jumpCenter = new Vector2();

        recalculateButtonPositions();
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        Vector2 viewportPosition = overlayViewport.unproject(new Vector2(screenX, screenY));

        if (viewportPosition.dst(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION) < Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS) {
            figurSpieler.enumLaufen = Enums.EnumLaufen.LAUFEND;
        } else if (viewportPosition.dst(jumpCenter) < Constants.BUTTON_RADIUS) {
            jumpPointer = pointer;
            gigaGal.jumpButtonPressed = true;
        } else if (viewportPosition.dst(moveLeftCenter) < Constants.BUTTON_RADIUS) {
            moveLeftPointer = pointer;
            gigaGal.leftButtonPressed = true;
        } else if (viewportPosition.dst(moveRightCenter) < Constants.BUTTON_RADIUS) {
            moveRightPointer = pointer;
            gigaGal.rightButtonPressed = true;
        }

        return super.touchDown(screenX, screenY, pointer, button);
    }
/*
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector2 viewportPosition = overlayViewport.unproject(new Vector2(screenX, screenY));

        if (pointer == moveLeftPointer && viewportPosition.dst(moveRightCenter) < Constants.BUTTON_RADIUS) {
            gigaGal.leftButtonPressed = false;
            moveLeftPointer = 0;

            moveRightPointer = pointer;
            gigaGal.rightButtonPressed = true;
        }

        if (pointer == moveRightPointer && viewportPosition.dst(moveLeftCenter) < Constants.BUTTON_RADIUS) {
            gigaGal.rightButtonPressed = false;
            moveRightPointer = 0;

            moveLeftPointer = pointer;
            gigaGal.leftButtonPressed = true;
        }

        return super.touchDragged(screenX, screenY, pointer);
    }

    public void render(SpriteBatch spriteBatch) {

        overlayViewport.apply();
        spriteBatch.setProjectionMatrix(overlayViewport.getCamera().combined);
        spriteBatch.begin();


        if (!Gdx.input.isTouched(jumpPointer)) {
            gigaGal.jumpButtonPressed = false;
            jumpPointer = 0;
        }

        if (!Gdx.input.isTouched(moveLeftPointer)) {
            gigaGal.leftButtonPressed = false;
            moveLeftPointer = 0;
        }

        if (!Gdx.input.isTouched(moveRightPointer)) {
            gigaGal.rightButtonPressed = false;
            moveRightPointer = 0;
        }


        Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.links,  Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION);
        Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.rechts,  Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION);
        Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.oben,  Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION);
        Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.unten,  Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION);

        spriteBatch.end();

    }

    public void recalculateButtonPositions() {

/*
        moveLeftCenter.set(Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4, Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS);
        moveRightCenter.set(Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 2, Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4);

        shootCenter.set(
                overlayViewport.getWorldWidth() - Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 2f,
                Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4
        );

        jumpCenter.set(
                overlayViewport.getWorldWidth() - Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4,
                Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS
        );

    }*/
}
