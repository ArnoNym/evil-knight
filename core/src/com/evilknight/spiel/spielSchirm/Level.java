package com.evilknight.spiel.spielSchirm;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.evilknight.spiel.spielSchirm.objekte.EigenerInputAdapter;
import com.evilknight.spiel.spielSchirm.objekte.Figur;
import com.evilknight.spiel.spielSchirm.objekte.FigurGegnerBauerA;
import com.evilknight.spiel.spielSchirm.objekte.FigurGegnerBogenschuetzeA;
import com.evilknight.spiel.spielSchirm.objekte.FigurGegnerSkelettA;
import com.evilknight.spiel.spielSchirm.objekte.FigurSpieler;
import com.evilknight.spiel.spielSchirm.objekte.Pfeil;
import com.evilknight.spiel.spielSchirm.overlays.ImLevelHUD;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class Level {
    public Viewport viewport;
    public Viewport overlayViewport;

    public FigurSpieler figurSpieler;
    EigenerInputAdapter eigenerInputAdapter;
    ImLevelHUD imLevelHUD;

    public Vector2 schwerkraft;

    //TODO Tiled
    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;
    private TiledMapTileLayer tiledMapKollisionLayer;
    private SpielSchirm spielSchirm;
    private int mapWidth;
    private int mapHeight;
    private int tilePixelWidth;
    private int tilePixelHeight;
    private int mapPixelWidth;
    private int mapPixelHeight;
    private int[] hintergrund = { 0, 1, 2 }; // don't allocate every frame!
    private int[] vordergrund = { 3 };    // don't allocate every frame!
    private List<Figur> figurenListe;
    private List<Pfeil> pfeilListe; //Todo Viieleich zu einer Objektliste ändenr oder so ..

    private long spielStratenZeit;
    private float spielStratenPauseDauer;

    public Level(SpielSchirm spielSchirm){
        viewport = new ExtendViewport(Konstanten.WELT_GROESSE, Konstanten.WELT_GROESSE);
        overlayViewport = new ExtendViewport(Konstanten.BILDSCHIRMTASTEN_VIEWPORT_GROESSE, Konstanten.BILDSCHIRMTASTEN_VIEWPORT_GROESSE);

        //TODO Tiled
        tiledMap = new TmxMapLoader().load(KonstantenAssets.TILESET_NAME);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, 1f);
        tiledMapKollisionLayer = (TiledMapTileLayer)tiledMap.getLayers().get(KonstantenAssets.TILED_KOLLISIONLAYER_INDEX);
        this.spielSchirm = spielSchirm;

        figurSpieler = new FigurSpieler(Konstanten.EVIL_KNIGHT_SPAWN_POSITION, this);
        figurenListe = new ArrayList();
        pfeilListe = new ArrayList();
        levelNeustart();

        eigenerInputAdapter = new EigenerInputAdapter(figurSpieler, viewport, overlayViewport);


        //tiledMapSpawnen();
        //figurenListe.add(new FigurGegnerZombieA(new Vector2(440, Konstanten.WELT_BODEN_HOEHE*3), this));
        //figurenListe.add(new FigurGegnerZombieFranken(new Vector2(460, Konstanten.WELT_BODEN_HOEHE*3), this));
        //figurenListe.add(new FigurGegnerSkelettA(Konstanten.SKELETT_A_SPAWN_POSITION, this));
        //figurenListe.add(new FigurGegnerSkelettA(new Vector2(660, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerBauerA(new Vector2(500, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerSkelettA(new Vector2(800, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerSkelettA(new Vector2(1200, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerBauerA(new Vector2(1800, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerBogenschuetzeA(new Vector2(660, Konstanten.WELT_BODEN_HOEHE*2), this));
        //figurenListe.add(new FigurGegnerBogenschuetzeA(new Vector2(530, 630), this));
        //pfeilListe.add(new Pfeil(new Vector2(300, 100),new Vector2(200, 100), this));

        mapWidth = tiledMap.getProperties().get("width", Integer.class);
        mapHeight = tiledMap.getProperties().get("height", Integer.class);
        tilePixelWidth = tiledMap.getProperties().get("tilewidth", Integer.class);
        tilePixelHeight = tiledMap.getProperties().get("tileheight", Integer.class);
        mapPixelWidth = mapWidth * tilePixelWidth;
        mapPixelHeight = mapHeight * tilePixelHeight;

        schwerkraft = Konstanten.WELT_SCHWERKRAFT;

        spielStratenZeit = TimeUtils.nanoTime();
        spielStratenPauseDauer = 1f;
    }

    public void update(float delta){
        if (!Werkzeuge.pause(spielStratenZeit, spielStratenPauseDauer)) {
            figurSpieler.update(delta);
            for (Figur f : figurenListe) {
                f.update(delta);
            }

            List<Pfeil> pfeileLoeschen = new ArrayList<Pfeil>(); //TODO Delayed removal Array verwenden
            for (Pfeil f : pfeilListe) {
                f.update(delta);
                if (Werkzeuge.berechneKollisionViereckUndPunktMitMitte(figurSpieler.getPositionMitte(), figurSpieler.getFigurHitboxBreiteUndHoehe(), f.getPosition())) {
                    figurSpieler.setLebenspunkte(figurSpieler.getLebenspunkte() - Konstanten.PFEIL_SCHADEN);
                    pfeileLoeschen.add(f);
                } else if (f.getPosition().y < -100) {
                    pfeileLoeschen.add(f);
                }
            }
            for (Pfeil f : pfeileLoeschen) {
                pfeilListe.remove(f);
            }
            //figurGegnerZombieA.update(delta);
            //figurGegnerZombieFranken.update(delta);

            lebenspunkteUeberpruefen();
            imLevelHUD.update(delta);
        }
    }

    public void render(SpriteBatch spriteBatch){

        viewport.apply();
        spriteBatch.setProjectionMatrix(viewport.getCamera().combined);

        tiledMapRenderer.setView(viewport.getCamera().combined, 0, 0, mapPixelWidth, mapPixelHeight);
        //tiledMapRenderer.setView(viewport.getCamera().combined, spielSchirm.getVerfolgungsKamera().kamera.position.x - viewport.getWorldWidth()/2, spielSchirm.getVerfolgungsKamera().kamera.position.y - viewport.getWorldHeight()/2, viewport.getWorldWidth(), viewport.getWorldHeight());
        //tiledMapRenderer.render();

        tiledMapRenderer.render(hintergrund);
        spriteBatch.begin();
        //spriteBatch.disableBlending(); verhindert transparente Texturen

        for(Figur f : figurenListe){
            f.render(spriteBatch);
        }
        for(Pfeil f : pfeilListe){
            f.render(spriteBatch);
        }

        figurSpieler.render(spriteBatch);
        spriteBatch.end();

        tiledMapRenderer.render(vordergrund);

        spriteBatch.begin();
        eigenerInputAdapter.render(spriteBatch);
        imLevelHUD.render(spriteBatch);
        spriteBatch.end();


    }

    public void levelNeustart () {
        if (Gdx.app.getType() == Application.ApplicationType.Android) { //Damit ich das auf dem Handy zeigen kann ohne dass man sofort verreckt
            figurSpieler.setMaxLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE *10);
            figurSpieler.setLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE *10);
        } else {
            figurSpieler.setMaxLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE);
            figurSpieler.setLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE);
        }
        figurSpieler.setSpringenSelbstschaden(0);
        figurSpieler.setPositionMitte(Konstanten.EVIL_KNIGHT_SPAWN_POSITION);
        figurSpieler.setBewegungsgeschwindigkeit(new Vector2(0,0));
        figurenListe.clear();
        tiledMapSpawnen();
        pfeilListe.clear();
        imLevelHUD = new ImLevelHUD(this, overlayViewport);
    }

    public void lebenspunkteUeberpruefen(){

        List<Figur> figurenLoeschen = new ArrayList<Figur>();
        for (Figur f : figurenListe){
            if(f.getLebenspunkte() <= 0){
                figurenLoeschen.add(f);
                figurSpieler.setMaxLebenspunkte(figurSpieler.getMaxLebenspunkte()+ Konstanten.SPIELER_LEBENSPUNKTE_MAX_ERHOEHEN_FUER_KILL);
                figurSpieler.setLebenspunkte(figurSpieler.getMaxLebenspunkte());
            }
        }
        for(Figur f : figurenLoeschen){
            figurenListe.remove(f);
        }

        if(figurSpieler.getLebenspunkte() <= 0){ //TODO FigurSpieler sterben lassen
            figurSpieler.setEnumRichtung(Enums.EnumRichtung.MITTE);
            levelNeustart();
        }
    }

    public void erschaffenPfeil(Vector2 positionSchuetze){
        pfeilListe.add(new Pfeil(positionSchuetze,figurSpieler.getPositionMitte(), this)); //TODO Fehlerbeheben, dass er mit jedme Pfeil abstürzt
    }

    private void tiledMapSpawnen () {
        TiledMapTileLayer tiledMapSpawnenLayer = (TiledMapTileLayer) tiledMap.getLayers().get(KonstantenAssets.TILED_SPAWNEN_INDEX);
        float tileGroesse = tiledMapSpawnenLayer.getTileHeight();//Höhe und breite sind gleich

        for (int j = 1; j < tiledMapSpawnenLayer.getWidth(); j++) {
            for (int i = 1; i < tiledMapSpawnenLayer.getHeight(); i++) {
                if (tiledMapSpawnenLayer.getCell(j, i).getTile().getProperties().containsKey("bogenschuetzeA")) {
                    figurenListe.add(new FigurGegnerBogenschuetzeA(new Vector2(((float)((j+0.5)*tileGroesse)), (i+1)*tileGroesse+KonstantenAssets.BOGENSCHUETZE_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2), this)); //+0,5 weil sonst die Position unten links gemeitn ist und TODO +1 weil es sont probleme mit der Kollision gab
                } else if (tiledMapSpawnenLayer.getCell(j, i).getTile().getProperties().containsKey("bauerA")) {
                    figurenListe.add(new FigurGegnerBauerA(new Vector2(((float)((j+0.5)*tileGroesse)), (i+1)*tileGroesse+KonstantenAssets.BAUER_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2), this));
                } else if (tiledMapSpawnenLayer.getCell(j, i).getTile().getProperties().containsKey("skelettA")) {
                    figurenListe.add(new FigurGegnerSkelettA(new Vector2(((float)((j+0.5)*tileGroesse)), (i+1)*tileGroesse+KonstantenAssets.SKELETT_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2), this));
                }
            }
        }
    }
    /*private void spawnen (TiledMapTileLayer.Cell cell, String spawnName, Figur figur) {
        if (cell.getTile().getProperties().containsKey(spawnName)) {
            figurenListe.add(new figur(new Vector2(), this));
            figurenListe.add(new FigurGegnerSkelettA(new Vector2(500, Konstanten.WELT_BODEN_HOEHE*2), this));
        }
    }*/








    public FigurSpieler getFigurSpieler(){
        return figurSpieler;
    }

    public List<Figur> getFigurenListe(){
        return figurenListe;
    }

    public TiledMapTileLayer getTiledMapKollisionLayer() {
        return tiledMapKollisionLayer;
    }

    public void setTiledMapKollisionLayer(TiledMapTileLayer tiledMapKollisionLayer) {
        this.tiledMapKollisionLayer = tiledMapKollisionLayer;
    }

    public TiledMap getTiledMap() {
        return tiledMap;
    }

    public void setTiledMap(TiledMap tiledMap) {
        this.tiledMap = tiledMap;
    }

    public TiledMapRenderer getTiledMapRenderer() {
        return tiledMapRenderer;
    }

    public void setTiledMapRenderer(TiledMapRenderer tiledMapRenderer) {
        this.tiledMapRenderer = tiledMapRenderer;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public int getTilePixelWidth() {
        return tilePixelWidth;
    }

    public void setTilePixelWidth(int tilePixelWidth) {
        this.tilePixelWidth = tilePixelWidth;
    }

    public int getTilePixelHeight() {
        return tilePixelHeight;
    }

    public void setTilePixelHeight(int tilePixelHeight) {
        this.tilePixelHeight = tilePixelHeight;
    }

    public int getMapPixelWidth() {
        return mapPixelWidth;
    }

    public void setMapPixelWidth(int mapPixelWidth) {
        this.mapPixelWidth = mapPixelWidth;
    }

    public int getMapPixelHeight() {
        return mapPixelHeight;
    }

    public void setMapPixelHeight(int mapPixelHeight) {
        this.mapPixelHeight = mapPixelHeight;
    }

    public void setFigurenListe(List<Figur> figurenListe) {
        this.figurenListe = figurenListe;
    }

    public List<Pfeil> getPfeilListe() {
        return pfeilListe;
    }

    public void setPfeilListe(List<Pfeil> pfeilListe) {
        this.pfeilListe = pfeilListe;
    }
}
