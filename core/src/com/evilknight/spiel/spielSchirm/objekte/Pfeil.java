package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;

/**
 * Created by LeonPB on 17.02.2018.
 */

public class Pfeil {
    private Level level;

    private Vector2 figurHitboxBreiteUndHoehe;
    private Vector2 position;

    //private Vector2 positionRendern;
    //private Vector2 positionSpitze;
    /*private Vector2 positionMitteModifikator;
    private Vector2 positionRendern; //Ist hier gleichzeitig auch position und die Position der Pfeilspitze
    private Vector2 positionRendernModifikator;
    private Vector2 positionSpitze;
    private Vector2 positionSpitzeModifikator; */

    private Vector2 bewegungsgeschwindigkeit;
    private float gewicht;

    private double ankatheteAbschuss;
    private double gegenkatheteAbschuss;
    private double hypothenuseAbschuss;
    private boolean xRichtungPositiv;
    private boolean yRichtungPositiv;
    private float winkelKippen;
    //Die Position des Pfeils muss modifiziert werden, da er nicht um seinen Mittelpunkt (wie es sein sollte) sondern um die Positio ganz unten links gedragt wird
    //Dazu wird die Position auf einer Laufbahn in Form eines Kreises je nach Winkel des Kippens bewegt
    //private float diagonaleDesKreises;
    //private Vector2 positionsPfeilKippenModifikator; //Die x und y Koodinaten sind nicht bei 0 und 1/2 , da das Bild des Pfeils noch leicht gedreht werden kann um den Abstand auf der x Achse zu erhöhen

    /*
    public Vector2 positionModifiziert; //Position verändert damit sich der Pfeil nicht um den Punkt unten Links dreht
    //private Vector2 positionStart; //Position von der der Pfeil geschossen wird um zu errechnen ob er weit genug geflogen ist
    //private Vector2 positionSpitze; //Spitze des Pfeils die zum berechnen des Schadens verwendet wird
    private Vector2 letzterFramePosition;
    private Vector2 zielPosition;

    private Vector2 positionRenderModifikator;
    private Vector2 positionSpitzeModifikator;


    private double ankatheteVar;
    private double gegenkatheteVar;
    private double hypothenuseVar;
    */

    public Pfeil(Vector2 position, Vector2 zielPosition, Level level) {
        this.position = position;
        this.level = level;

        xRichtungPositiv = (zielPosition.x- position.x) > 0;
        yRichtungPositiv = (zielPosition.y- position.y) > 0;

        figurHitboxBreiteUndHoehe =  new Vector2(KonstantenAssets.PFEIL_A_FIGUR_HITBOX_BREITE_UND_HOEHE);
        /*positionRendernModifikator = new Vector2(KonstantenAssets.PFEIL_A_POSITION_RENDERN_MODIFIKATOR);
        positionSpitzeModifikator = new Vector2(KonstantenAssets.PFEIL_A_POSITION_SPITZE_MODIFIKATOR);
        if (xRichtungPositiv) {
            positionRendern = new Vector2(position.x + positionRendernModifikator.x, position.y + positionRendernModifikator.y);
            positionSpitze = new Vector2(position.x + positionSpitzeModifikator.x, position.y + positionSpitzeModifikator.y);
        } else {
            positionRendern = new Vector2(position.x - positionRendernModifikator.x, position.y - positionRendernModifikator.y);
            positionSpitze = new Vector2(position.x - positionSpitzeModifikator.x, position.y - positionSpitzeModifikator.y);
        }
        */


        ankatheteAbschuss = zielPosition.x - position.x;
        gegenkatheteAbschuss = zielPosition.y - position.y;
        hypothenuseAbschuss = Math.sqrt(Math.abs(Math.pow(ankatheteAbschuss,2) + Math.pow(gegenkatheteAbschuss,2)));
        winkelKippen =(float) Math.toDegrees(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss));


        /*float zwischenGegenkathete = zielPosition.y - positionSpitze.y;
        Vector2 zwischenBewegungsgeschwindigkeit = new Vector2();
        float zeitBenoetigtFuerStrecke = 0f;
        //for (int i = 0; i<10; i++){
            zwischenBewegungsgeschwindigkeit.y = (float) (Math.cos(Math.atan(gegenkatheteAbschuss / zwischenGegenkathete)) * (xRichtungPositiv ? Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : (-Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT)));
            zeitBenoetigtFuerStrecke = Math.abs(zwischenGegenkathete / zwischenBewegungsgeschwindigkeit.y);
            zwischenGegenkathete = (zielPosition.y - positionSpitze.y) + (((zeitBenoetigtFuerStrecke * Math.abs(Konstanten.WELT_SCHWERKRAFT.y)) * zeitBenoetigtFuerStrecke) / 2);//(zeitBenoetigtFuerStreckeZwischenAnkathete*(zeitBenoetigtFuerStreckeZwischenAnkathete+1))/2*Math.abs(Konstanten.WELT_SCHWERKRAFT.y);
        //} */

        gewicht = Konstanten.PFEIL_GEWICHT;
        bewegungsgeschwindigkeit = new Vector2();
        bewegungsgeschwindigkeit.x = (float) (Math.cos(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)) * (xRichtungPositiv ? Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : (-Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT)));
        bewegungsgeschwindigkeit.y = (float) (Math.sin(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)) * (xRichtungPositiv ? Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : (-Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT)));
        float zeitBenoetigtFuerStrecke = (float)Math.abs(hypothenuseAbschuss / Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT);
        bewegungsgeschwindigkeit.y = bewegungsgeschwindigkeit.y + (zeitBenoetigtFuerStrecke*Math.abs(Konstanten.WELT_SCHWERKRAFT.y*gewicht))/2;

        //diagonaleDesKreises = (float)(Math.cos(Math.atan(figurHitboxBreiteUndHoehe.y / figurHitboxBreiteUndHoehe.x)) * figurHitboxBreiteUndHoehe.x); //Da der Kreis der um den Kasten des Bildes vom pfeil gezeichnet werden soll //
        /*positionsPfeilKippenModifikator = new Vector2();
        positionsPfeilKippenModifikator.x = (float) Math.sin(((diagonaleDesKreises - figurHitboxBreiteUndHoehe.x)/2)/diagonaleDesKreises * 180);
        positionsPfeilKippenModifikator.y = (float) Math.sin(((diagonaleDesKreises - figurHitboxBreiteUndHoehe.y)/2)/diagonaleDesKreises * 180);
        positionModifiziert = new Vector2(); */

        /*
        if (xRichtungPositiv) {
            winkelKippen =(float)(yRichtungPositiv ? Math.toDegrees(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)) : 360 - Math.toDegrees(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)));
        } else {
            winkelKippen = (float)(yRichtungPositiv ? 360 - Math.toDegrees(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)) : Math.toDegrees(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss)));
        }/*
/*
        bewegungsgeschwindigkeit = new Vector2();
        bewegungsgeschwindigkeit.x = (float) (Math.cos( Math.toDegrees(Math.tan(gegenkatheteAbschuss / ankatheteAbschuss)) ) * (xRichtungPositiv ? Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : (-Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT)));
        bewegungsgeschwindigkeit.y = (float) (Math.sin( Math.toDegrees(Math.tan(gegenkatheteAbschuss / ankatheteAbschuss)) ) * (yRichtungPositiv ? Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : (-Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT)));
*/
        /*
        if (Math.abs(zielPosition.y-position.y) < Math.abs(zielPosition.x-position.x)) {
            bewegungsgeschwindigkeit.x = zielPosition.x - positionSpitze.x < 0 ? -Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : +Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT;
            bewegungsgeschwindigkeit.y = (zielPosition.y - positionSpitze.y) / (zielPosition.x - positionSpitze.x) * bewegungsgeschwindigkeit.x;
        } else {
            bewegungsgeschwindigkeit.x = zielPosition.x - positionSpitze.x < 0 ? -Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT : +Konstanten.PFEIL_BEWEGUNGSGESCHWINDIGKEIT;
            bewegungsgeschwindigkeit.y = (zielPosition.y - positionSpitze.y) / (zielPosition.x - positionSpitze.x) * bewegungsgeschwindigkeit.x;
        }
        */
    }

    public void update(float delta){
        //setPosition(new Vector2(getPosition().x + delta * bewegungsgeschwindigkeitKonstante * bewegungsgeschwindigkeit.x, getPosition().y + delta * bewegungsgeschwindigkeitKonstante * bewegungsgeschwindigkeit.y));
        bewegungsgeschwindigkeit.y += Konstanten.WELT_SCHWERKRAFT.y * gewicht * delta;

        position.x += delta * bewegungsgeschwindigkeit.x;
        position.y += delta * bewegungsgeschwindigkeit.y;

        //positionModifiziert.x = position.x + (float) (Math.cos(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss + Math.toDegrees(positionsPfeilKippenModifikator.x))) * (xRichtungPositiv ? diagonaleDesKreises/2 : (-diagonaleDesKreises/2)));
        //positionModifiziert.y = position.y + (float) (Math.cos(Math.atan(gegenkatheteAbschuss / ankatheteAbschuss + Math.toDegrees(positionsPfeilKippenModifikator.y))) * (xRichtungPositiv ? diagonaleDesKreises/2 : (-diagonaleDesKreises/2)));
        //position.y = (float) (position.y + Math.sin(position.x));
        /*
        positionRendern.x = position.x-positionRendernModifikator.x;
        //positionRendern.x = positionRendern.x + (float)Math.cos(winkelKippen * Math.PI/180) * diagonaleDesKreises/2; //(float)Math.cos(winkelKippen * Math.PI/180) * diagonaleDesKreises; //Pro 180Grad muss die x-Koordinate einmal ganz links und einmal ganz rechts sein. 90Grad ergibt eins und wird dann mit der Diagonalen Multipliziert um immer die gesamte Diagonale entlang zu laufen
        positionRendern.y = position.y-positionRendernModifikator.y;
        //positionRendern.y = positionRendern.y + (float)Math.cos(winkelKippen * Math.PI/180) * diagonaleDesKreises/2; //(float)Math.cos((winkelKippen+90) * Math.PI/180) * diagonaleDesKreises;
        positionSpitze.x = position.x-positionSpitzeModifikator.x;
        positionSpitze.y = position.y-positionSpitzeModifikator.y;*/

        //Winkel für das Kippen berechnen
        winkelKippen = (float) Math.toDegrees(Math.atan(bewegungsgeschwindigkeit.y / bewegungsgeschwindigkeit.x));
    }

    public void render(SpriteBatch spriteBatch){
        TextureRegion region;
        region = Assets.instance.pfeilAssets.pfeil;
        //Werkzeuge.zeichneTexturRegion(spriteBatch, region, positionRendern, xRichtungPositiv);
        //float ankatheteAbschuss = Math.abs(zielPosition.x-position.x);
        //float gegenkatheteAbschuss = Math.abs(zielPosition.y-position.y);
        //float hypothenuseAbschuss = Math.sqrt(Math.abs( Math.pow(ankatheteAbschuss,2) + Math.pow(gegenkatheteAbschuss,2) )));


        spriteBatch.draw(
                region.getTexture(),
                position.x, //horizontalSpiegeln ? x+region.getRegionWidth() : x, kann gelöscht werden aber ich finde den Befehl gut, vielleicht erinnert man sich so daran
                position.y,
                0,
                0,
                region.getRegionWidth(), //horizontalSpiegeln ? -region.getRegionWidth(): region.getRegionWidth(),
                region.getRegionHeight(),
                1,
                1,
                //(int) Math.acos((double)gegenkatheteAbschuss / ankatheteAbschuss), //TODO Pfeil deren funktioniert noch nicht
                winkelKippen-(xRichtungPositiv ? 315 : 45),
                //MathUtils.cos(bewegungsgeschwindigkeit.x / bewegungsgeschwindigkeit.y)
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                xRichtungPositiv,
                false);
    }

    public Vector2 getPosition() {
        return position;
    }
}
