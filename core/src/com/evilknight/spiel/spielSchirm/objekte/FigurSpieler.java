package com.evilknight.spiel.spielSchirm.objekte;

//import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
        import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class FigurSpieler extends Figur{

    private float springenSelbstschaden;

    public FigurSpieler(Vector2 position, Level level){
        super(position, level);

        setFigurHitboxBreiteUndHoehe(KonstantenAssets.RITTER_Si_FIGUR_HITBOX_BREITE_UND_HOEHE);
        setPositionRenderModifikator(KonstantenAssets.RITTER_Si_POSITION_RENDER_MODIFIKATOR);
        setPositionRendern(new Vector2(getPositionMitte().x-KonstantenAssets.RITTER_Si_POSITION_RENDER_MODIFIKATOR.x, getPositionMitte().y-KonstantenAssets.RITTER_Si_POSITION_RENDER_MODIFIKATOR.y));
        setEnumRichtung(Enums.EnumRichtung.MITTE);
        setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);
        setEnumSpringen(Enums.EnumSpringen.BODEN);
        setEnumAngreifen(Enums.EnumAngreifen.NICHT_ANGREIFEN);

        setSchlagenHitboxBreiteUndHohe(KonstantenAssets.RITTER_Si_SCHLAGEN_HITBOX_BREITE_UND_HOEHE);
        setSchlagenAnimationsdauer(Konstanten.RITTER_Si_SCHLAGEN_ANIMATIONSDAUER);
        setAktionBeenden(false);
        setSchlagenBereitsGeschlagen(false);

        setLaufenGeschwindigkeit(Konstanten.EVIL_KNIGHT_BEWEGUNGSGESCHWINDIGKEIT);

        setLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE); //Lebenspuntke hier sind eigentlich egal, da das in level unter Neustart eh nochmal neu egsetzt wird
        setMaxLebenspunkte(Konstanten.RITTER_Si_LEBENSPUNKTE);
        springenSelbstschaden = 0;
        setSchlagenSchaden(Konstanten.RITTER_SI_SCHLAGEN_SCHADEN);
        setZeitBisIdle(Konstanten.RITTER_Si_ZEIT_BIS_IDLE);
        setSchlagenSchadenVerzoegerung(KonstantenAssets.RITTER_Si_SCHLAGEN_SCHADEN_VERZOEGERUNG);
        setSchlagenNachDiesemSchlagBeenden(false);

        setGewicht(Konstanten.RITTER_Si_GEWICHT);
        setSprungkraft(Konstanten.RITTER_Si_SPRUNGKRAFT);
    }

    public void update(float delta){
        altePositionMerken();

        schwerkraft(delta);
        //springenBerechnen(delta); //beinhaltet Schwerkraft

        if (getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
            if (getEnumRichtung() == Enums.EnumRichtung.LINKS) {
                setBewegungsgeschwindigkeit(new Vector2(-1, getBewegungsgeschwindigkeit().y));
                setPositionMitte(new Vector2(getPositionMitte().x -= delta * getLaufenGeschwindigkeit() , getPositionMitte().y));
            } else if (getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
                setBewegungsgeschwindigkeit(new Vector2(1, getBewegungsgeschwindigkeit().y));
                setPositionMitte(new Vector2(getPositionMitte().x += delta * getLaufenGeschwindigkeit() , getPositionMitte().y));
            }
        }else if (getAktionBeenden() && TimeUtils.nanoTime() - getAktionBeendenZeit() >= getZeitBisIdle()/MathUtils.nanoToSec){
            setBewegungsgeschwindigkeit(new Vector2(0, getBewegungsgeschwindigkeit().y));
            setEnumRichtung(Enums.EnumRichtung.MITTE);
        }

        super.update(delta);
        updateSchlagen();
        if (getEnumSpringen() == Enums.EnumSpringen.BODEN) { springenSelbstschaden=0; }
    }

    public void springen () {
        setEnumSpringen(Enums.EnumSpringen.SPRINGEND);
        setLebenspunkte(getLebenspunkte() - springenSelbstschaden);
        springenSelbstschaden += Konstanten.SPIELER_SPRINGEN_SELBSTSCHADEN;
        setBewegungsgeschwindigkeit(new Vector2(getBewegungsgeschwindigkeit().x, getSprungkraft()));
    }

    public void laufenAnfangen(Enums.EnumRichtung enumRichtung){
        setEnumRichtung(enumRichtung);
        setLaufenStartZeit(TimeUtils.nanoTime());
        aktionAnfangen();
        setEnumLaufen(Enums.EnumLaufen.LAUFEND);
    }
    public void laufenBeenden(){
        setAktionBeendenZeit(TimeUtils.nanoTime());
        aktionBeenden();
        setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);
    }

    public void angreifenAnfangen(Enums.EnumRichtung enumRichtung){
        setSchlagenStartZeit(TimeUtils.nanoTime()); //Damit das korrekte Bild der Animation gewählt wird
        aktionAnfangen(); //Damit nicht zur Idle Animation gewechselt wird
        setEnumAngreifen(Enums.EnumAngreifen.ANGREIFEN); //Damit der Angriff gerendert wird
        setSchlagenBereitsGeschlagen(false);
        setSchlagenNachDiesemSchlagBeenden(false);
        setEnumRichtung(enumRichtung); //Damit der Angriff in die richtige Richtung gerendert wird
    }
    public void angreifenBeenden(){
        setSchlagenNachDiesemSchlagBeenden(true);

    }
    public void updateSchlagen(){
        if(TimeUtils.nanoTime() - getSchlagenStartZeit() >= getSchlagenSchadenVerzoegerung()/MathUtils.nanoToSec && getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
            if(!getSchlagenBereitsGeschlagen()) {
                schlagenAusfuehren(true, false); //Schlagen und Schaden berechnen
                setSchlagenBereitsGeschlagen(true);
            }
            if(TimeUtils.nanoTime() - getSchlagenStartZeit() >= getSchlagenAnimationsdauer()/MathUtils.nanoToSec) {
                if (!getSchlagenNachDiesemSchlagBeenden()) {
                    setSchlagenStartZeit(TimeUtils.nanoTime());
                    setSchlagenBereitsGeschlagen(false);
                } else {
                    aktionBeenden();
                    setEnumAngreifen(Enums.EnumAngreifen.NICHT_ANGREIFEN);
                 }
            }
        }
    }
    public void schlagenAnimationBeendenUeberpruefen(){
        if(TimeUtils.nanoTime() - getSchlagenStartZeit() >= getSchlagenAnimationsdauer()/MathUtils.nanoToSec && getSchlagenNachDiesemSchlagBeenden() && getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
            aktionBeenden();
            setEnumAngreifen(Enums.EnumAngreifen.NICHT_ANGREIFEN);
            setSchlagenBereitsGeschlagen(false); //Beides Vermutlich nicht unbeding tnötig aber es könnte etwas schief gehen wenn das Update zu früh gecallt wird oder so (keien Ahnugn ob das geht)
            setSchlagenNachDiesemSchlagBeenden(false); //Beides Vermutlich nicht unbeding tnötig aber es könnte etwas schief gehen wenn das Update zu früh gecallt wird oder so (keien Ahnugn ob das geht)
        }
    }

    private void aktionAnfangen(){
        setAktionBeenden(false);
    }
    private void aktionBeenden(){
        setAktionBeendenZeit(TimeUtils.nanoTime());
        setAktionBeenden(true);
    }

    public void render(SpriteBatch spriteBatch){
        float vergangeneSchlagenZeit = Werkzeuge.secondsSince(getSchlagenStartZeit());
        TextureRegion region = Assets.instance.ritterSiAssets.mitteStehenAnimation.getKeyFrame(vergangeneSchlagenZeit);

        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() != Enums.EnumRichtung.MITTE){
            float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
            region = Assets.instance.ritterSiAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN && /* enumLaufen == Enums.EnumLaufen.NICHT_LAUFEND && */ getEnumRichtung() != Enums.EnumRichtung.MITTE) {
            region = Assets.instance.ritterSiAssets.linksSchlagenAnimation.getKeyFrame(vergangeneSchlagenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN ){
            region = Assets.instance.ritterSiAssets.linksStehen;
            if (getEnumRichtung() == Enums.EnumRichtung.MITTE){
                region = Assets.instance.ritterSiAssets.mitteStehenAnimation.getKeyFrame(vergangeneSchlagenZeit); //TODO Timer für Idletanimationen
            }
        }

        //Werkzeuge.zeichneTexturRegion(spriteBatch, region, getPositionRendern(), getEnumRichtung()== Enums.EnumRichtung.RECHTS);
        spriteBatch.draw(
        region.getTexture(),
                getPositionRendern().x, //horizontalSpiegeln ? x+region.getRegionWidth() : x, kann gelöscht werden aber ich finde den Befehl gut, vielleicht erinnert man sich so daran
                getPositionRendern().y,
                0,
                0,
                region.getRegionWidth()*2, //horizontalSpiegeln ? -region.getRegionWidth(): region.getRegionWidth(),
                region.getRegionHeight()*2,
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                getEnumRichtung()== Enums.EnumRichtung.RECHTS,
                false);
    }

    public void setSpringenSelbstschaden(float springenSelbstschaden) {
        this.springenSelbstschaden = springenSelbstschaden;
    }
}
