package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 15.02.2018.
 */

public class FigurGegnerBauerA extends FigurGegner {

    public FigurGegnerBauerA(Vector2 position, Level level) {
        super(position, level);
        setFigurHitboxBreiteUndHoehe(KonstantenAssets.BAUER_A_FIGUR_HITBOX_BREITE_UND_HOEHE);

        setPositionRenderModifikator(KonstantenAssets.BAUER_A_POSITIONS_RENDER_MODIFIKATOR);
        setPositionRendern(new Vector2(position.x- KonstantenAssets.BAUER_A_POSITIONS_RENDER_MODIFIKATOR.x, position.y-KonstantenAssets.BAUER_A_POSITIONS_RENDER_MODIFIKATOR.y));

        setSichtweite(Konstanten.BAUER_A_SICHTWEITE);
        setLaufenGeschwindigkeit(Konstanten.BAUER_A_BEWEGUNGSGESCHWINDIGKEIT);
        setLebenspunkte(Konstanten.BAUER_A_LEBENSPUNKTE);
        setGewicht(1);

        setSchlagenHitboxBreiteUndHohe(KonstantenAssets.BAUER_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE);
        setSchlagenPositionModifikator(KonstantenAssets.BAUER_A_SCHLAGEN_POSITION_MODIFIKATOR);
        setSchlagenAnimationsdauer(Konstanten.BAUER_A_SCHLAGEN_ANIMATIONSDAUER);
        setSchlagenSchaden(Konstanten.BAUER_A_SCHLAGEN_SCHADEN);
        setSchlagenSchadenVerzoegerung(Konstanten.BAUER_A_SCHLAGEN_VERZOEGERUNG);

        setKannStuermen(Konstanten.BAUER_A_KANN_STUERMEN);
        setStuermenDauer(Konstanten.BAUER_A_STUERMEN_DAUER);
        setStuermenMindestentfernung(Konstanten.BAUER_A_STUERMEN_MINDESTENTFERNUNG);
        setStuermenReichweite(Konstanten.BAUER_A_STUERMEN_REICHWEITE);
        setStuermenBewegungsgeschwindigkeit(Konstanten.BAUER_A_STUERMEN_BEWEGUNGSGESCHWINDIGKEIT);
        setSprungkraft(Konstanten.BAUER_A_SPRUNGKRAFT);
        setStuermenSchaden(Konstanten.BAUER_A_STUERMEN_SCHADEN);
    }

    public void update(float delta){
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch spriteBatch){
        float vergangeneSchlagenZeit = Werkzeuge.secondsSince(getSchlagenStartZeit());
        float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
        float vergangeneStuermenZeit = Werkzeuge.secondsSince(getStuermenStartZeit());
        TextureRegion region;
        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN) {
            region = Assets.instance.bauerAAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN) {
            region = Assets.instance.bauerAAssets.linksSchlagenAnimation.getKeyFrame(vergangeneSchlagenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.STUERMEN) {
            region = Assets.instance.bauerAAssets.linksStuermenAnimation.getKeyFrame(vergangeneStuermenZeit);
        }else {
            region = Assets.instance.bauerAAssets.linksStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }
        Werkzeuge.zeichneTexturRegion(spriteBatch, region, getPositionRendern(), getEnumRichtung() == Enums.EnumRichtung.RECHTS);
    }
}
