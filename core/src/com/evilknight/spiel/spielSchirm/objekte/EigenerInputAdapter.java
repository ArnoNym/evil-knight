package com.evilknight.spiel.spielSchirm.objekte;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 01.11.2017.
 */

public class EigenerInputAdapter extends InputAdapter{

    public final Viewport overlayViewport;
    public Viewport viewport;
    public FigurSpieler figurSpieler;
    private Vector2 moveLeftCenter;
    private Vector2 moveRightCenter;
    private Vector2 shootCenter;
    private Vector2 jumpCenter;
    private int linksLaufenPointer;
    private int rechtsLaufenPointer;
    private int obenLaufenPointer;
    private int untenLaufenPointer;
    private int linksSchlagenPointer;
    private int rechtsSchlagenPointer;

    private boolean tasteGedrueckt;
    private boolean laufenBeenden;
    private boolean schlagenBeenden;
    private boolean springenBeenden;

    /*
    public EigenerInputAdapter() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) { //TODO machen, dass man durchgängig schlagen kann
        fS.bildschirmDown(screenX, screenY, pointer, button);
        return true;
    }
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        fS.bildschirmUp(screenX, screenY, pointer, button);
        return true;
    }
    */


    public EigenerInputAdapter(FigurSpieler figurSpieler, Viewport viewport, Viewport overlayViewport) {
        Gdx.input.setInputProcessor(this);
        this.figurSpieler = figurSpieler;
        this.viewport = viewport;
        this.overlayViewport = overlayViewport;

        moveLeftCenter = new Vector2();
        moveRightCenter = new Vector2();
        shootCenter = new Vector2();
        jumpCenter = new Vector2();

        linksLaufenPointer = 0;
        rechtsLaufenPointer = 0;
        obenLaufenPointer = 0;
        untenLaufenPointer = 0;
        linksSchlagenPointer = 0;
        rechtsSchlagenPointer = 0;

        tasteGedrueckt = false;
    }

    @Override
    public boolean keyDown(int keycode){
        if(keycode == Input.Keys.A) {
            figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
        }else if(keycode == Input.Keys.D) {
            figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
        }
        if(keycode == Input.Keys.W) {
            figurSpieler.springen();
            //figurSpieler.springenBeginnenSobaldMoeglich();
        }
        return true;
    }
    @Override
    public boolean keyUp(int keycode){
        if(!Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)) { //Damit die Figur beim gleichzeitgen drücken nicht aufhört zu laufen wenn eine der Tasten losgelassen wird
            if (keycode == Input.Keys.A || keycode == Input.Keys.D) {
                figurSpieler.laufenBeenden();
            }
        }
        if(!Gdx.input.isKeyPressed(Input.Keys.W)) {
            if (keycode == Input.Keys.W) {
                figurSpieler.springenBeenden();
            }
        }
        return true;
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        tasteGedrueckt = false;

        Vector2 weltKlick = viewport.unproject(new Vector2(screenX, screenY));
        Vector2 weltKlickOverlay = overlayViewport.unproject(new Vector2(screenX, screenY));

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                linksLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                rechtsLaufenPointer = pointer;
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.springen();
                //figurSpieler.springenBeginnenSobaldMoeglich();
                obenLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }
        }

        if(!tasteGedrueckt){
            if(weltKlick.x < figurSpieler.getPositionMitte().x){
                figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                linksSchlagenPointer = pointer;
            }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                rechtsSchlagenPointer = pointer;
            }
        }
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer){
        tasteGedrueckt = false;
        laufenBeenden = true;
        schlagenBeenden = true;
        springenBeenden = true;

        if(pointer == linksLaufenPointer){linksLaufenPointer =0;}
        if(pointer == rechtsLaufenPointer){rechtsLaufenPointer =0;}
        if(pointer == obenLaufenPointer){obenLaufenPointer =0;}
        if(pointer == untenLaufenPointer){untenLaufenPointer =0;}
        if(pointer == linksSchlagenPointer){linksSchlagenPointer =0;}
        if(pointer == rechtsSchlagenPointer){rechtsSchlagenPointer =0;}

        Vector2 weltKlick = viewport.unproject(new Vector2(screenX, screenY));
        Vector2 weltKlickOverlay = overlayViewport.unproject(new Vector2(screenX, screenY));

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                linksLaufenPointer = pointer;
                laufenBeenden = false;
                if (figurSpieler.getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND) {
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                }
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                rechtsLaufenPointer = pointer;
                laufenBeenden = false;
                if (figurSpieler.getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND) {
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                }
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {
                obenLaufenPointer = pointer;
                figurSpieler.springen();
                //springenBeenden = false;
                //figurSpieler.springenBeginnenSobaldMoeglich();
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }
        }

        if(!tasteGedrueckt){
            if(weltKlick.x < figurSpieler.getPositionMitte().x){
                linksSchlagenPointer = pointer;
                schlagenBeenden = false;
                if(figurSpieler.getEnumAngreifen() != Enums.EnumAngreifen.ANGREIFEN){
                    figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                }else{
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                }
            }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                rechtsSchlagenPointer = pointer;
                schlagenBeenden = false;
                if(figurSpieler.getEnumAngreifen() != Enums.EnumAngreifen.ANGREIFEN){
                    figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                }else{
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                }
            }
        }

        if(laufenBeenden && !Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)){
            figurSpieler.laufenBeenden();}
        if(schlagenBeenden){
            figurSpieler.angreifenBeenden();}
        if(springenBeenden && !Gdx.input.isKeyPressed(Input.Keys.W)){
            figurSpieler.springenBeenden();}

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            aktionBerechnen(linksLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(rechtsLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(obenLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(untenLaufenPointer, pointer, weltKlick, false, Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, weltKlickOverlay);
            aktionBerechnen(linksSchlagenPointer, pointer, weltKlick, true, new Vector2(), new Vector2(), weltKlickOverlay);
            aktionBerechnen(rechtsSchlagenPointer, pointer, weltKlick, true, new Vector2(), new Vector2(), weltKlickOverlay);
        }
        return super.touchDragged(screenX, screenY, pointer);
    }

    public void aktionBerechnen(int eigenerPointer, int pointer, Vector2 weltKlick, boolean mussKeineTasteTreffen, Vector2 untenLinksPositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        if(eigenerPointer == pointer && !Werkzeuge.berechneKollisionViereckUndPunkt(untenLinksPositionDesVierecks, breiteUndHoeheDesVierecks, positionDesPunktes) || mussKeineTasteTreffen){
            tasteGedrueckt = false;

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {
                if(figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                    rechtsLaufenPointer = 0;
                    linksLaufenPointer = pointer;
                }else{
                    figurSpieler.angreifenBeenden();
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.LINKS);
                    linksLaufenPointer = pointer;
                }
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {
                if(figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
                    figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                    linksLaufenPointer = 0;
                    rechtsLaufenPointer = pointer;
                }else{
                    figurSpieler.angreifenBeenden();
                    figurSpieler.laufenAnfangen(Enums.EnumRichtung.RECHTS);
                    rechtsLaufenPointer = pointer;
                }
                tasteGedrueckt = true;
            }

            if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)
                    && figurSpieler.getEnumSpringen() != Enums.EnumSpringen.SPRINGEND) {
                figurSpieler.angreifenBeenden();
                figurSpieler.springenBeginnenSobaldMoeglich();
                obenLaufenPointer = pointer;
                tasteGedrueckt = true;
            } else if (Werkzeuge.berechneKollisionViereckUndPunkt(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION, Konstanten.BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE, positionDesPunktes)) {

                untenLaufenPointer = pointer;
                tasteGedrueckt = true;
            }

            if(!tasteGedrueckt){
                if(weltKlick.x < figurSpieler.getPositionMitte().x){
                    if(figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
                        figurSpieler.setEnumRichtung(Enums.EnumRichtung.LINKS);
                        linksSchlagenPointer = pointer;
                    }else {
                        figurSpieler.laufenBeenden();
                        figurSpieler.angreifenAnfangen(Enums.EnumRichtung.LINKS);
                        linksSchlagenPointer = pointer;
                    }
                }else if(weltKlick.x >= figurSpieler.getPositionMitte().x){
                    if(figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
                        figurSpieler.setEnumRichtung(Enums.EnumRichtung.RECHTS);
                        rechtsSchlagenPointer = pointer;
                    }else {
                        figurSpieler.laufenBeenden();
                        figurSpieler.angreifenAnfangen(Enums.EnumRichtung.RECHTS);
                        rechtsSchlagenPointer = pointer;
                    }
                }
            }
        }
    }


  /*  @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        Vector2 weltKlickOverlay = overlayViewport.unproject(new Vector2(screenX, screenY));

        if (weltKlickOverlay.dst(Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION) < Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS) {
            figurSpieler.laufenBeenden();
        } else if (weltKlickOverlay.dst(Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION) < Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS) {
            figurSpieler.laufenBeenden();
        } else if (weltKlickOverlay.dst(Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION) < Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS) {

        } else if (weltKlickOverlay.dst(Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION) < Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS) {

        } else {
            figurSpieler.angreifenBeenden();
        }

        return true;
    }*/
    /*
        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            Vector2 viewportPosition = overlayViewport.unproject(new Vector2(screenX, screenY));

            if (pointer == moveLeftPointer && viewportPosition.dst(moveRightCenter) < Constants.BUTTON_RADIUS) {
                gigaGal.leftButtonPressed = false;
                moveLeftPointer = 0;

                moveRightPointer = pointer;
                gigaGal.rightButtonPressed = true;
            }

            if (pointer == moveRightPointer && viewportPosition.dst(moveLeftCenter) < Constants.BUTTON_RADIUS) {
                gigaGal.rightButtonPressed = false;
                moveRightPointer = 0;

                moveLeftPointer = pointer;
                gigaGal.leftButtonPressed = true;
            }

            return super.touchDragged(screenX, screenY, pointer);
        }
    */
    public void render(SpriteBatch spriteBatch) {

        overlayViewport.apply();
        spriteBatch.setProjectionMatrix(overlayViewport.getCamera().combined);

        if(!Gdx.input.isKeyPressed(Input.Keys.W) && !Gdx.input.isTouched(obenLaufenPointer)) {
            figurSpieler.springenBeenden();
            obenLaufenPointer = 0;
        }
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            if (figurSpieler.getEnumSpringen() != Enums.EnumSpringen.SPRINGEND) {
                if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                    figurSpieler.springenBeginnenSobaldMoeglich();
                } else if (Gdx.input.isTouched(obenLaufenPointer)) {
                    figurSpieler.springenBeginnenSobaldMoeglich();
                    obenLaufenPointer = 0;
                }
            }
        }

        if (!Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D)
                && figurSpieler.getEnumLaufen() == Enums.EnumLaufen.LAUFEND) { //Damit nicht andauernd das Laufen Beendet wird was den idle Timer resettet und somit nie die Idle Animation eingeleitet wird
            if (!Gdx.input.isTouched(linksLaufenPointer)) {
                figurSpieler.laufenBeenden();
                linksLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(rechtsLaufenPointer)) {
                figurSpieler.laufenBeenden();
                rechtsLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(obenLaufenPointer)) {
                figurSpieler.springenBeenden();
                obenLaufenPointer = 0;
            }
            if (!Gdx.input.isTouched(untenLaufenPointer)) {

                untenLaufenPointer = 0;
            }
        }
        if (figurSpieler.getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN) { //Damit nicht andauernd der ANgriff Beendet wird was den idle Timer resettet und somit nie die Idle Animation eingeleitet wird
            if (!Gdx.input.isTouched(linksSchlagenPointer)) {
                figurSpieler.angreifenBeenden();
                linksSchlagenPointer = 0;
            }
            if (!Gdx.input.isTouched(rechtsSchlagenPointer)) {
                figurSpieler.angreifenBeenden();
                rechtsSchlagenPointer = 0;
            }
        }

        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.links, Konstanten.BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.rechts, Konstanten.BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.oben, Konstanten.BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION);
            Werkzeuge.drawTextureRegion(spriteBatch, Assets.instance.bildschirmtastenAssets.unten, Konstanten.BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION);
        }

    }

    public void recalculateButtonPositions() {

/*
        moveLeftCenter.set(Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4, Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS);
        moveRightCenter.set(Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 2, Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4);

        shootCenter.set(
                overlayViewport.getWorldWidth() - Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 2f,
                Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4
        );

        jumpCenter.set(
                overlayViewport.getWorldWidth() - Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS * 3 / 4,
                Konstanten.BILDSCHIRMTASTEN_TASTE_RADIUS
        );
*/
    }
}
