package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 29.10.2017.
 */

public class FigurGegnerZombieA extends FigurGegner {

    public FigurGegnerZombieA(Vector2 position, Level level) {
        super(position, level);
        setPositionRenderModifikator(KonstantenAssets.ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR);
        setPositionMitteModifikator(KonstantenAssets.ZOMBIE_A_POSITIONS_MITTE_MODIFIKATOR);
        //setPositionRendern(new Vector2(position.x- KonstantenAssets.ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR.x, position.y-KonstantenAssets.ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR.y));
        setPositionMitte(new Vector2(position.x+KonstantenAssets.ZOMBIE_A_POSITIONS_MITTE_MODIFIKATOR.x, position.y+KonstantenAssets.ZOMBIE_A_POSITIONS_MITTE_MODIFIKATOR.y));
        setFigurHitboxBreiteUndHoehe(Konstanten.ZOMBIE_A_FIGUR_HITBOX_BREITE_UND_HOEHE);
        setSchlagenHitboxBreiteUndHohe(Konstanten.ZOMBIE_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE);

        setSichtweite(Konstanten.ZOMBIE_A_SICHTWEITE);
        setLaufenGeschwindigkeit(Konstanten.ZOMBIE_A_BEWEGUNGSGESCHWINDIGKEIT);
        setLebenspunkte(Konstanten.ZOMBIE_A_LEBENSPUNKTE);
        setSchlagenSchaden(Konstanten.ZOMBIE_A_SCHLAGEN_SCHADEN);
        setSchlagenSchadenVerzoegerung(Konstanten.ZOMBIE_A_SCHLAGEN_VERZOEGERUNG);
        setGewicht(1);
    }

    public void update(float delta){
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch spriteBatch){
        /*TextureRegion region = Assets.instance.ritterSAssets.rechtsStehen;*/
        /* TextureRegion region = Assets.instance.zombieAAssets.linksStehen;
        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
            float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
            region = Assets.instance.zombieAAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }
        Werkzeuge.drawTextureRegion(spriteBatch, region, getPositionRendern()); */
        TextureRegion region = Assets.instance.zombieAAssets.linksStehen;
        spriteBatch.draw(region, getPositionRendern().x, getPositionRendern().y, region.getRegionWidth(), region.getRegionHeight());

    }
}
