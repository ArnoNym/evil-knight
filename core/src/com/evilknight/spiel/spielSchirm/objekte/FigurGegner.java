package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 30.10.2017.
 */

public abstract class FigurGegner extends Figur { //Alle FigurGegner sind FigurGegner(Klasse) und alle FigurGegner(Klasse) sind Figuren(Klasse)

    private Vector2 sichtweite;

    private boolean kannStuermen;
    private long stuermenStartZeit;
    private long stuermenDauer;
    private float stuermenMindestentfernung;
    private float stuermenReichweite;
    private float stuermenBewegungsgeschwindigkeit;
    private float stuermenSchaden;

    private boolean kannBlocken;

    private boolean kannSchiessen;
    private long schiessenStartZeit;
    private float schiessenMindestentfernung;
    private float schiessenReichweite;
    private float schiessenSchaden;

    public FigurGegner(Vector2 position, Level level){
        super(position, level);
        setKannStuermen(false);  //Sachen die nicht alle Gegner können werden standardmäßig auf false gesetzt und können dann in zB GegnerBauer geändert werden wenn er es können soll
        setKannSchiessen(false);
        setKannBlocken(false);
        setPauseStartzeitVonUpdate(0);

        setSchiessenBereitsGeschossen(true);
        setSchlagenBereitsGeschlagen(true);
        /*setSchlagenStartZeit((long) -getSchlagenAnimationsdauer() -1); //TODO Ich glaube das ist unnötig
        setStuermenStartZeit((long) -getStuermenDauer() -1);
        setSchiessenStartZeit((long) -getSchiessenAnimationsdauer() -1);*/
    }

    public void update(final float delta) {
        altePositionMerken(); //Für die Kollision
        schwerkraft(delta); //TODO FRAGE Wenn ich die Schwerkraft vor Super.Update setzte sollte es doch auch funktionieren. Jedoch können die Gegner dann durch Wände laufen

        //updateSchlagenHitboxPauseGegner();
        if (!getSchlagenBereitsGeschlagen() && !Werkzeuge.pause(getSchlagenStartZeit(), getSchlagenSchadenVerzoegerung())) { //Damit der Schaden mit einer verzögerung ausgeführt wid (Wenn es bei der Animation passt)
            schlagenAusfuehren(false, true);
            setSchlagenBereitsGeschlagen(true);
        }
        if (kannSchiessen && !getSchiessenBereitsGeschossen() && !Werkzeuge.pause(getSchiessenStartZeit(), getSchiessenSchadenVerzoegerung())) { //Damit der Schaden mit einer verzögerung ausgeführt wid (Wenn es bei der Animation passt)
            schiessenAusfuehren();
            setSchiessenBereitsGeschossen(true);
        }
        if (!abfrageSchlagenGrade() && !abfrageSchiessenGrade() && !abfrageStuermenGrade() && !abfrageBlockenGrade()) { //Damit Animationen zuende ausgeführt werden könne. pauseStartzeitVonUpdate = TimeUtils.nanoTime(); um eine Pause einzuleiten
            //setSchlagenBereitsGeschlagen(true);
            setEnumAngreifen(Enums.EnumAngreifen.NICHT_ANGREIFEN);
            setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);

            Enums.EnumRichtung siehtSpielerInRichtung = siehtSpieler(getPositionMitte(), sichtweite, false);
            if (siehtSpielerInRichtung == Enums.EnumRichtung.NEIN) {//Wenn er ihn nciht sieht bleibe stehen
                setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);
            } else {
                setEnumRichtung(siehtSpielerInRichtung);
                if (getKannSchiessen()) { //Wenn Schiessen kann kann die Figur aktuell nicht stürmen oder zuschlagen
                    if(siehtSpieler(getPositionMitte(), new Vector2(getSchiessenMindestentfernung(), getSichtweite().y), true) == Enums.EnumRichtung.NEIN){
                        schiessenAnfangen();
                    }else{
                        laufen(delta, true); //TODO Weglaufen
                    }
                } else if (kannStuermen && siehtSpieler(getPositionMitte(), new Vector2(getStuermenMindestentfernung(), getFigurHitboxBreiteUndHoehe().y/2), true) == Enums.EnumRichtung.NEIN) { //Nur für die die anstürmen können
                    stuermenAnfangen();
                }else {
                    if (spielerInAngriffsreichweite(siehtSpielerInRichtung, getSchlagenHitboxBreiteUndHohe())) { //Ist er in Reichweite für einen Angriff bzw in diesem Fall "Schlagen"?
                        schlagen();
                    } else { //Wenn Nein laufe zu ihm
                        if (kannBlocken && spielerGreiftAn()) {
                             blocken();
                        } else {
                            laufen(delta, false);
                            if (getLetzterFrameGegnerKollisionX()) {
                                springen();
                            }
                        }
                    }
                }
            }
            //;}},pauseStartzeitVonUpdate); //Ende der update Pause während der Animationen zuende ausgeführt werden können
        }
        if (getEnumLaufen() == Enums.EnumLaufen.STUERMEN) {
            stuermen(delta);
        }

        super.update(delta);
    }
    public void updateSchlagenHitboxPauseGegner(){
        if(TimeUtils.nanoTime() - getSchlagenStartZeit() >= getSchlagenSchadenVerzoegerung()/ MathUtils.nanoToSec && getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN){
            if(!getSchlagenBereitsGeschlagen()) {
                schlagenAusfuehren(true, false); //Schlagen und Schaden berechnen
                setSchlagenBereitsGeschlagen(true);
            }
        }
    }

    //Anbfragen an den Gegener //Nicht zu evrwechseln mit Sinnen des Gegners weiter unten
    public boolean abfrageSchlagenGrade(){
        return Werkzeuge.pause(getSchlagenStartZeit(), getSchlagenAnimationsdauer());
    }
    public boolean abfrageStuermenGrade(){
        return Werkzeuge.pause(getStuermenStartZeit(), getStuermenDauer());
    }
    public boolean abfrageSchiessenGrade(){
        return Werkzeuge.pause(getSchiessenStartZeit(), getSchiessenAnimationsdauer());
    }
    public boolean abfrageBlockenGrade(){
        return Werkzeuge.pause(getBlockenStartZeit(), getBlockenDauer());
    }

    //Aktionen des Gegners
    public void blocken(){
        setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);
        setEnumAngreifen(Enums.EnumAngreifen.BLOCKEN);
        setBlockenStartZeit(TimeUtils.nanoTime());
    }
    public void schiessenAnfangen(){
        setSchiessenStartZeit(TimeUtils.nanoTime());
        setSchiessenBereitsGeschossen(false);
        //getLevel().getPfeilListe().add(new Pfeil(getPositionMitte(), getLevel().getFigurSpieler().getPositionMitte(), getLevel()));
    }
    public void schiessenAusfuehren(){
        //getLevel().getPfeilListe().add(new Pfeil(getPositionMitte(), getLevel().getFigurSpieler().getPositionMitte(), getLevel()));
        getLevel().erschaffenPfeil(getPositionSchiessen());
    }

    public void stuermenAnfangen(){
        setStuermenStartZeit(TimeUtils.nanoTime());
        setEnumLaufen(Enums.EnumLaufen.STUERMEN);
    }
    public void stuermen(float delta){
        //schlagenAusfuehren(false, true, delta);
        if(getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            setPositionMitte(new Vector2(getPositionMitte().x -= delta * getStuermenBewegungsgeschwindigkeit(), getPositionMitte().y));
            setBewegungsgeschwindigkeit(new Vector2(-1, getBewegungsgeschwindigkeit().y)); //TODO für die Kollision glaube ich? Vereinfachen?
            Vector2 schadenPositionUntenLinks = schadenPositionUntenLinks_LinksVonFigurBerechnen(getSchlagenHitboxBreiteUndHohe());
            schadenAusfuehren(schadenPositionUntenLinks, getSchlagenHitboxBreiteUndHohe(), getStuermenSchaden(), true, false, true, delta);
        }else{
            setPositionMitte(new Vector2(getPositionMitte().x += delta * getStuermenBewegungsgeschwindigkeit(), getPositionMitte().y));
            setBewegungsgeschwindigkeit(new Vector2(+1, getBewegungsgeschwindigkeit().y));
            Vector2 schadenPositionUntenLinks = schadenPositionUntenLinks_RechtsVonFigurBerechnen();
            schadenAusfuehren(schadenPositionUntenLinks, getSchlagenHitboxBreiteUndHohe(), getStuermenSchaden(), true, false, true, delta);
        }
    }
    public void schlagen(){
        setEnumLaufen(Enums.EnumLaufen.NICHT_LAUFEND);
        setEnumAngreifen(Enums.EnumAngreifen.ANGREIFEN);
        setSchlagenBereitsGeschlagen(false);//Nachdem die Update Pause (wird direkt hier drunter gestartet) ausgelaufen ist wird am Anfang von Update auf diesen Wert geprüft und dann evtl gechlagen
        setSchlagenStartZeit(TimeUtils.nanoTime());
        setBewegungsgeschwindigkeit(new Vector2(0, getBewegungsgeschwindigkeit().y));
    }
    public void springen(){
        setBewegungsgeschwindigkeit(new Vector2(getBewegungsgeschwindigkeit().x, getSprungkraft()));
    }
    public void laufen(float delta, boolean weglaufen){
        /* if (getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
            setLaufenStartZeit(TimeUtils.nanoTime());
        }*/
        if ((!weglaufen && getEnumRichtung() == Enums.EnumRichtung.LINKS) || (weglaufen && getEnumRichtung() == Enums.EnumRichtung.RECHTS)) {
            setPositionMitte(new Vector2(getPositionMitte().x -= delta * getLaufenGeschwindigkeit(), getPositionMitte().y));
            setEnumLaufen(Enums.EnumLaufen.LAUFEND);
            setBewegungsgeschwindigkeit(new Vector2((-1), getBewegungsgeschwindigkeit().y));
        } else {
            setPositionMitte(new Vector2(getPositionMitte().x += delta * getLaufenGeschwindigkeit(), getPositionMitte().y));
            setEnumLaufen(Enums.EnumLaufen.LAUFEND);
            setBewegungsgeschwindigkeit(new Vector2(1, getBewegungsgeschwindigkeit().y));
        }
        if (weglaufen) {
            setEnumRichtung(getEnumRichtung()== Enums.EnumRichtung.LINKS ? Enums.EnumRichtung.RECHTS : Enums.EnumRichtung.LINKS);
        }
    }

    //Sinne des Gegners
    public boolean spielerGreiftAn(){ //TODO Gegnerschlag einführen
        boolean spielerGreiftAn = false;
        if (getLevel().getFigurSpieler().getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN
                && ( (getLevel().getFigurSpieler().getEnumRichtung() == Enums.EnumRichtung.RECHTS && getEnumRichtung() == Enums.EnumRichtung.LINKS)
                || (getLevel().getFigurSpieler().getEnumRichtung() == Enums.EnumRichtung.LINKS && getEnumRichtung() == Enums.EnumRichtung.RECHTS) )) {
            spielerGreiftAn = true;
        } else {
            spielerGreiftAn = false;
        }
        return spielerGreiftAn; //Den return brauche ich momentan nciht aber evtl kann man die FUnktion noch woanders für nutzen
    }

    public boolean spielerInAngriffsreichweite(Enums.EnumRichtung enumRichtung, Vector2 angriffHitboxBreiteUndHoehe){
        boolean kannErreichenY = getLevel().figurSpieler.getPositionMitte().y + getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().y / 2 <= getPositionMitte().y + getFigurHitboxBreiteUndHoehe().y/2 + getSchlagenHitboxBreiteUndHohe().y/2 //Das mit der getSchlagenHitboxBreiteUndHohe ist eine Vereifnachung, da ich nicht genau weiß wo der Schöag hin soll
                && getLevel().figurSpieler.getPositionMitte().y + getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().y / 2 >= getPositionMitte().y - getFigurHitboxBreiteUndHoehe().y/2 - getSchlagenHitboxBreiteUndHohe().y/2;
        if(enumRichtung == Enums.EnumRichtung.LINKS && getPositionMitte().x - getFigurHitboxBreiteUndHoehe().x/2 - angriffHitboxBreiteUndHoehe.x * Konstanten.GEGNER_SCHLAGEN_ENTFERNUNG_MODIFIKATOR <= getLevel().figurSpieler.getPositionMitte().x + getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().x/2
                && kannErreichenY){
            return true;
        }else if(enumRichtung == Enums.EnumRichtung.RECHTS && getPositionMitte().x + getFigurHitboxBreiteUndHoehe().x/2 + angriffHitboxBreiteUndHoehe.x * Konstanten.GEGNER_SCHLAGEN_ENTFERNUNG_MODIFIKATOR >= getLevel().figurSpieler.getPositionMitte().x - getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().x/2
                && kannErreichenY){
            return true;
        }else {
            return false;
        }
    }
    public Enums.EnumRichtung siehtSpieler(Vector2 gegnerPositionMitte, Vector2 sichtweite, boolean auchYPruefen) { //Evtl noch Höhe einführen , float sichttiefeY, float sichthoeheY
        boolean siehtY = getLevel().figurSpieler.getPositionMitte().y + getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().y / 2 <= getPositionMitte().y + sichtweite.y
                && getLevel().figurSpieler.getPositionMitte().y + getLevel().figurSpieler.getFigurHitboxBreiteUndHoehe().y / 2 >= getPositionMitte().y - sichtweite.y;
        if((getLevel().getFigurSpieler().getPositionMitte().x >= gegnerPositionMitte.x-sichtweite.x) &&   //Überprüfen ob er ihn sieht
                (getLevel().getFigurSpieler().getPositionMitte().x <= gegnerPositionMitte.x+sichtweite.x)
                && (siehtY || !auchYPruefen)){
            if(getLevel().getFigurSpieler().getPositionMitte().x < gegnerPositionMitte.x){ //Überprüfen in welche Richtung
                return Enums.EnumRichtung.LINKS;
            }else if(getLevel().getFigurSpieler().getPositionMitte().x > gegnerPositionMitte.x){
                return Enums.EnumRichtung.RECHTS;
            }else{
                return Enums.EnumRichtung.NEIN; //TODo Er sieht ihn ja eigtnlich schon aber soll halt nicht auf der Stelle laufen -> Überarbeiten
            }
        }else{
            return Enums.EnumRichtung.NEIN;
        }
    }

    public void renderFigurGegner(SpriteBatch spriteBatch, Assets assets){ //TODO Idee war das redern der Gegner zu standardisier. Da ich aber sehr unterschiedliche Gegener haben werde ist das vielleicht nicht so gut. Außerdme hab ich das auch nicht vernünftig hinbekommen
        /*TextureRegion region = Assets.instance.ritterSAssets.rechtsStehen;*//*
        float vergangeneSchlagenZeit = Werkzeuge.secondsSince(getSchlagenStartZeit());
        float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
        TextureRegion region = Assets.instance.skelettAAssets.linksStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            region = Assets.instance.skelettAAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND&& getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            region = Assets.instance.skelettAAssets.rechtsLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            region = Assets.instance.skelettAAssets.linksSchiessenAnimation.getKeyFrame(vergangeneSchlagenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            region = Assets.instance.skelettAAssets.rechtsSchlagenAnimation.getKeyFrame(vergangeneSchlagenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            region = Assets.instance.skelettAAssets.linksStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            region = Assets.instance.skelettAAssets.rechtsStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }
        Werkzeuge.drawTextureRegion(spriteBatch, region, getPositionRendern());*/
    }





    public boolean getKannStuermen() {
        return kannStuermen;
    }

    public void setKannStuermen(boolean kannStuermen) {
        this.kannStuermen = kannStuermen;
    }

    public float getStuermenBewegungsgeschwindigkeit() {
        return stuermenBewegungsgeschwindigkeit;
    }

    public void setStuermenBewegungsgeschwindigkeit(float stuermenBewegungsgeschwindigkeit) {
        this.stuermenBewegungsgeschwindigkeit = stuermenBewegungsgeschwindigkeit;
    }

    public float getStuermenSchaden() {
        return stuermenSchaden;
    }

    public void setStuermenSchaden(float stuermenSchaden) {
        this.stuermenSchaden = stuermenSchaden;
    }

    public float getStuermenMindestentfernung() {
        return stuermenMindestentfernung;
    }

    public void setStuermenMindestentfernung(float stuermenMindestentfernung) {
        this.stuermenMindestentfernung = stuermenMindestentfernung;
    }

    public float getStuermenReichweite() {
        return stuermenReichweite;
    }

    public void setStuermenReichweite(float stuermenReichweite) {
        this.stuermenReichweite = stuermenReichweite;
    }

    public long getStuermenStartZeit() {
        return stuermenStartZeit;
    }

    public void setStuermenStartZeit(long stuermenStartZeit) {
        this.stuermenStartZeit = stuermenStartZeit;
    }

    public long getStuermenDauer() {
        return stuermenDauer;
    }

    public void setStuermenDauer(long stuermenDauer) {
        this.stuermenDauer = stuermenDauer;
    }

    public boolean getKannSchiessen() {
        return kannSchiessen;
    }

    public void setKannSchiessen(boolean kannSchiessen) {
        this.kannSchiessen = kannSchiessen;
    }

    public long getSchiessenStartZeit() {
        return schiessenStartZeit;
    }

    public void setSchiessenStartZeit(long schiessenStartZeit) {
        this.schiessenStartZeit = schiessenStartZeit;
    }

    public float getSchiessenMindestentfernung() {
        return schiessenMindestentfernung;
    }

    public void setSchiessenMindestentfernung(float schiessenMindestentfernung) {
        this.schiessenMindestentfernung = schiessenMindestentfernung;
    }

    public float getSchiessenReichweite() {
        return schiessenReichweite;
    }

    public void setSchiessenReichweite(float schiessenReichweite) {
        this.schiessenReichweite = schiessenReichweite;
    }

    public float getSchiessenSchaden() {
        return schiessenSchaden;
    }

    public void setSchiessenSchaden(float schiessenSchaden) {
        this.schiessenSchaden = schiessenSchaden;
    }

    public boolean getKannBlocken() {
        return kannBlocken;
    }

    public void setKannBlocken(boolean kannBlocken) {
        this.kannBlocken = kannBlocken;
    }

    public Vector2 getSichtweite() {
        return sichtweite;
    }

    public void setSichtweite(Vector2 sichtweite) {
        this.sichtweite = sichtweite;
    }
}
