package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 17.02.2018.
 */

public class FigurGegnerBogenschuetzeA extends FigurGegner {

    public FigurGegnerBogenschuetzeA(Vector2 position, Level level) {
        super(position, level);

        setFigurHitboxBreiteUndHoehe(KonstantenAssets.BOGENSCHUETZE_A_FIGUR_HITBOX_BREITE_UND_HOEHE);

        setPositionRenderModifikator(KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_RENDER_MODIFIKATOR);
        setPositionRendern(new Vector2(position.x- KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_RENDER_MODIFIKATOR.x, position.y-KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_RENDER_MODIFIKATOR.y));
        setPositionSchiessen(new Vector2(position.x + KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_SCHIESSEN_MODIFIKATOR.x, position.y + KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_SCHIESSEN_MODIFIKATOR.y));

        setSchiessenAnimationsdauer(Konstanten.BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER);
        setSchiessenSchadenVerzoegerung(KonstantenAssets.BOGENSCHUETZE_A_SCHIESSEN_SCHADEN_VERZOEGERUNG);
        setSchiessenBereitsGeschossen(true);

        setSichtweite(Konstanten.BOGENSCHUETZE_A_SICHTWEITE);
        setLaufenGeschwindigkeit(Konstanten.BOGENSCHUETZE_A_BEWEGUNGSGESCHWINDIGKEIT);
        setSprungkraft(Konstanten.BOGENSCHUETZE_A_SPRUNGKRAFT);
        setLebenspunkte(Konstanten.BOGENSCHUETZE_A_LEBENSPUNKTE);
        setGewicht(1);

        setKannSchiessen(Konstanten.BOGENSCHUETZE_A_KANN_SCHIESSEN);
        setSchiessenMindestentfernung(Konstanten.BOGENSCHUETZE_A_SCHIESSEN_MINDESTENTFERNUNG);
        setSchiessenReichweite(Konstanten.BOGENSCHUETZE_A_SCHIESSEN_REICHWEITE);
        setSchiessenSchaden(Konstanten.BOGENSCHUETZE_A_SCHLAGEN_SCHADEN);
    }

    public void update(float delta){
        super.update(delta);
        setPositionSchiessen(new Vector2(getPositionMitte().x + KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_SCHIESSEN_MODIFIKATOR.x * (getEnumRichtung()== Enums.EnumRichtung.LINKS ? (-1) : 1), getPositionMitte().y + KonstantenAssets.BOGENSCHUETZE_A_POSITIONS_SCHIESSEN_MODIFIKATOR.y));
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        float vergangeneSchiessenZeit = Werkzeuge.secondsSince(getSchiessenStartZeit());
        float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
        TextureRegion region;
        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
            region = Assets.instance.bogenschuetzeAAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(abfrageSchiessenGrade()) {
            region = Assets.instance.bogenschuetzeAAssets.linksSchiessenAnimation.getKeyFrame(vergangeneSchiessenZeit);
        }else {
            region = Assets.instance.bogenschuetzeAAssets.linksStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }
        Werkzeuge.zeichneTexturRegion(spriteBatch, region, getPositionRendern(), getEnumRichtung() == Enums.EnumRichtung.RECHTS);
    }
}
