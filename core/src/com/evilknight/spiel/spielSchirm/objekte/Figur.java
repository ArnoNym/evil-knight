package com.evilknight.spiel.spielSchirm.objekte;

/**
 * Created by LeonPB on 01.11.2017.
 */

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 30.10.2017.
 */

public abstract class Figur { //Alle FigurGegner sind FigurGegner(Klasse) und alle FigurGegner(Klasse) sind Figuren(Klasse)
    private Level level;
    //private Vector2 position; //Siehe Notizen
    private Vector2 positionMitte; //Siehe Notizen
    private Vector2 letzterFramePosition;
    private Vector2 positionRendern; //Siehe Notizen
    private Vector2 positionSchiessen;
    private Vector2 positionMitteModifikator;
    private Vector2 positionRenderModifikator;
    private Vector2 figurHitboxBreiteUndHoehe;
    private Vector2 schlagenHitboxBreiteUndHohe;
    private Vector2 schlagenPositionModifikator;
    private float schlagenAnimationsdauer;
    private float schiessenAnimationsdauer;
    private long blockenStartZeit;
    private float blockenBeginnenAnimationsDauer;
    private float blockenBeendenAnimationsDauer;
    private float blockenDauer;

    private Enums.EnumLaufen enumLaufen;
    private Enums.EnumRichtung enumRichtung;
    private Enums.EnumAngreifen enumAngreifen;
    private Enums.EnumSpringen enumSpringen;

    private long laufenStartZeit;
    private long schlagenStartZeit;
    private long letzteAktionZeit; //Damit nach soundso vielen Sekunden nach einer Aktion die Idle Animation beginnt

    private long aktionBeendenZeit; //Damit nach soundso vielen Sekunden nach einer Aktion die Idle Animation beginnt
    private boolean aktionBeenden;
    private boolean moechteSpringen;

    private long pauseStartzeitVonUpdate; //während der Animationen zuende ausgeführt werden können -> float ist immer in Sekunden
    private float pauseDauerVonUpdate;
    private long pauseSchlagenStartzeit; //während der Animationen zuende ausgeführt werden können -> float ist immer in Sekunden
    private float pauseSchlagenDauer;
    private float schlagenSchadenVerzoegerung;
    private float schiessenSchadenVerzoegerung;
    private int schlagID; //Wird benutzt um die Schläge zu identifizieren und nur den aktuellsten Schlagen des Spielers auszuführen
    private boolean schlagenBereitsGeschlagen;
    private boolean schiessenBereitsGeschossen;

    private boolean schlagenNachDiesemSchlagBeenden; //Wenn true wird der Schlagen nach Beendigung des letzten Schlages beendet

    private boolean letzterFrameGegnerKollisionX; //Braucht eigentlich nur die GegnerFigur aber es muss in Figur verwendet werdne, deshalb hier
    private float laufenGeschwindigkeit;
    private Vector2 bewegungsgeschwindigkeit;
    private float gewicht;
    private float sprungkraft;
    private float lebenspunkte;
    private float maxLebenspunkte;
    private float schlagenSchaden;
    private float zeitBisIdle;

    //Tiled
    private float tileGroesse;

    public Figur(Vector2 position, Level level){
        this.level = level;
        this.positionMitte = position;

        enumAngreifen = Enums.EnumAngreifen.NICHT_ANGREIFEN;
        enumLaufen = Enums.EnumLaufen.NICHT_LAUFEND;

        bewegungsgeschwindigkeit = new Vector2(0, 0);
        schlagenPositionModifikator = new Vector2(0, 0);

        letzterFrameGegnerKollisionX = false;
        letzterFramePosition = new Vector2();

        tileGroesse = level.getTiledMapKollisionLayer().getTileHeight();
    }

    public void update(float delta){
        kollisionsabfrage();
        //setPositionRendern(new Vector2(getPosition().x-getPositionRenderModifikator().x, getPosition().y-getPositionRenderModifikator().y));
        //setPositionMitte(new Vector2(getPosition().x-getPositionMitteModifikator().x, getPosition().y-getPositionMitteModifikator().y));
        //TODO Warum funktiuoniert das nicht mit Settern??
        positionRendern.x = positionMitte.x-positionRenderModifikator.x;
        positionRendern.y = positionMitte.y-positionRenderModifikator.y;
    }

    public abstract void render(SpriteBatch spriteBatch);

    public void springenBeginnenSobaldMoeglich(){ //Wir aktuell nur vom Spieler benutzt
        moechteSpringen = true;
    }
    public void springenBeenden(){ //Wir aktuell nur vom Spieler benutzt
        moechteSpringen = false;
        setEnumSpringen(Enums.EnumSpringen.FALLEND);
    }
    public void schwerkraft(float delta){
        bewegungsgeschwindigkeit.y += Konstanten.WELT_SCHWERKRAFT.y / gewicht * delta; //WELT_SCHWERKRAFT ist negativ
        setPositionMitte(new Vector2(getPositionMitte().x, getPositionMitte().y + bewegungsgeschwindigkeit.y * delta));
        //position.y += bewegungsgeschwindigkeit.y * delta;
    }
    public void springenBerechnen(float delta){
        /*TODO Tiled
        if(positionMitte.y < Konstanten.WELT_BODEN_HOEHE){
            position.y = Konstanten.WELT_BODEN_HOEHE;
            enumSpringen = Enums.EnumSpringen.BODEN;
            bewegungsgeschwindigkeit.y = 0;
        }
        */
        if(getEnumSpringen() != Enums.EnumSpringen.BODEN){
            schwerkraft(delta);
        }else if(getEnumSpringen() == Enums.EnumSpringen.BODEN && moechteSpringen){
            setEnumSpringen(Enums.EnumSpringen.SPRINGEND);
            /*if (this.getClass() == FigurSpieler.class) {
                setLebenspunkte(getLebenspunkte() - Konstanten.SPIELER_SPRINGEN_SELBSTSCHADEN);
            }*/
            setBewegungsgeschwindigkeit(new Vector2(getBewegungsgeschwindigkeit().x, sprungkraft));
        }
    }
    public void kollisionsabfrage(){
  /*      if (getPositionMitte().y - getFigurHitboxBreiteUndHoehe().y/2 <= tileGroesse) {
            setPositionMitte(new Vector2(getPositionMitte().x, tileGroesse + getFigurHitboxBreiteUndHoehe().y/2));
        } //TODO Blocken damit man nicht aus dem Spiel fällt (auch x Achse) mti Kollision zusammenlegen udn auch nochmal überarbeiten
*/
        boolean kollisionX = false;
        if((enumLaufen == Enums.EnumLaufen.LAUFEND && enumRichtung == Enums.EnumRichtung.LINKS) || getBewegungsgeschwindigkeit().x < 0) {
            kollisionX = level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x-figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y+figurHitboxBreiteUndHoehe.y/2-1)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_RECHTS);
            kollisionX |= level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x-figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_RECHTS);
            kollisionX |= level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x-figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y-figurHitboxBreiteUndHoehe.y/2+1)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_RECHTS);
        }else if((enumLaufen == Enums.EnumLaufen.LAUFEND && enumRichtung == Enums.EnumRichtung.RECHTS) || getBewegungsgeschwindigkeit().x > 0) {
            kollisionX = level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x+figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y+figurHitboxBreiteUndHoehe.y/2-1)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_LINKS);
            kollisionX |= level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x+figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_LINKS);
            kollisionX |= level.getTiledMapKollisionLayer().getCell((int)((positionMitte.x+figurHitboxBreiteUndHoehe.x/2)/tileGroesse), (int)((positionMitte.y-figurHitboxBreiteUndHoehe.y/2+1)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_LINKS);
        }
        boolean kollisionY = false;
        if(getBewegungsgeschwindigkeit().y < 0 && bewegtInTile(Enums.EnumRichtung.OBEN)){
            Vector2 hitbox;
            if (this.getClass() == FigurSpieler.class && getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {//Der spieler steht mit seiner Hitbox und nicht der Mitte seiner Figut. Außerdem kann er kurzzeitig wie dieser eine Kojote über die Klippe hinaus in der Luft laufen. Machen viele Spiele und fühlt sich richtig an
                hitbox = new Vector2(figurHitboxBreiteUndHoehe.x + Konstanten.SPIELER_KOYOTE, figurHitboxBreiteUndHoehe.y + Konstanten.SPIELER_KOYOTE);
            } else if (this.getClass() == FigurSpieler.class || getEnumLaufen() == Enums.EnumLaufen.LAUFEND) {
                hitbox = figurHitboxBreiteUndHoehe;
            } else {
                hitbox = new Vector2(0,0);
            }
            kollisionY = level.getTiledMapKollisionLayer().getCell((int) ((getPositionMitte().x + hitbox.x) / tileGroesse), (int) ((getPositionMitte().y - hitbox.y / 2) / tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_OBEN)
                    || level.getTiledMapKollisionLayer().getCell((int) (getPositionMitte().x / tileGroesse), (int) ((getPositionMitte().y - figurHitboxBreiteUndHoehe.y / 2) / tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_OBEN)
                    || level.getTiledMapKollisionLayer().getCell((int) ((getPositionMitte().x - hitbox.x) / tileGroesse), (int) ((getPositionMitte().y - hitbox.y / 2) / tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_OBEN);
        }else if(getBewegungsgeschwindigkeit().y > 0) {
        kollisionY = level.getTiledMapKollisionLayer().getCell((int)((getPositionMitte().x)/tileGroesse), (int)((getPositionMitte().y+figurHitboxBreiteUndHoehe.y/2)/tileGroesse)).getTile().getProperties().containsKey(KonstantenAssets.BLOCK_UNTEN);
        }
        setPositionMitte(new Vector2(kollisionX ? letzterFramePosition.x : positionMitte.x, kollisionY ? kollisionsabfrageBerechneYHoeheUnten() : getPositionMitte().y));
        letzterFrameGegnerKollisionX=kollisionX;
        if (kollisionY) {
            setBewegungsgeschwindigkeit(new Vector2(bewegungsgeschwindigkeit.x, 0));
            setEnumSpringen(Enums.EnumSpringen.BODEN);
        }
    }
    public float kollisionsabfrageBerechneYHoeheUnten(){ //Wenn man mit der letztenFramePosition arbeitet fällt er am Ende sehr "gedämpft"
        int anzahlGanzerTiles = (int) (getLetzterFramePosition().y-figurHitboxBreiteUndHoehe.y/2) / level.getTilePixelHeight();
        return (anzahlGanzerTiles * level.getTilePixelHeight()+figurHitboxBreiteUndHoehe.y/2); //die Anzahl der ganzen Tiles die unter der aktuellen Posisiton sind * die höhe dieser Tiles
    }
    public boolean bewegtInTile(Enums.EnumRichtung richtungVomTileAusGesehen){
        Vector2 a = new Vector2((int)((positionMitte.x+figurHitboxBreiteUndHoehe.x/2)/ tileGroesse), (int)((positionMitte.y-figurHitboxBreiteUndHoehe.y/2)/tileGroesse));
        Vector2 b = new Vector2((int)((letzterFramePosition.x+figurHitboxBreiteUndHoehe.x/2)/ tileGroesse), (int)((letzterFramePosition.y-figurHitboxBreiteUndHoehe.y/2)/tileGroesse));

        //TODO Diesen Part auskommentieren um den Bug zu verhindern wo man durch das seitwerte Springen in ein Tile mit Block_Oben nochmal Springen konnte. Den fand ich nämlich eigentlich ganz charmant
        if (richtungVomTileAusGesehen == Enums.EnumRichtung.LINKS || richtungVomTileAusGesehen == Enums.EnumRichtung.RECHTS) {
            int modX = richtungVomTileAusGesehen == Enums.EnumRichtung.LINKS ? -1 : richtungVomTileAusGesehen == Enums.EnumRichtung.RECHTS ? 1 : 0;
            return (a.x + modX == b.x);
        } else if (richtungVomTileAusGesehen == Enums.EnumRichtung.OBEN || richtungVomTileAusGesehen == Enums.EnumRichtung.UNTEN){
            int modY = richtungVomTileAusGesehen == Enums.EnumRichtung.UNTEN ? -1 : richtungVomTileAusGesehen == Enums.EnumRichtung.OBEN ? 1 : 0;
            return (a.y + modY == b.y);
        }
        return !(a.x == b.x && a.y == b.y);
    }

    public void altePositionMerken(){
        letzterFramePosition = positionMitte;
    }

    public void schlagenAusfuehren(boolean sollGegnerTreffen, boolean sollSpielerTreffen, float... deltaFuerProSekunde){ //Ohne schlagId für die Gegner
        //boolean proSekunde = deltaFuerProSekunde.length > 0;

        if (getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            Vector2 schadenPositionUntenLinks = schadenPositionUntenLinks_LinksVonFigurBerechnen(getSchlagenHitboxBreiteUndHohe());
            schadenAusfuehren(schadenPositionUntenLinks, getSchlagenHitboxBreiteUndHohe(), getSchlagenSchaden(), true, sollGegnerTreffen, sollSpielerTreffen, deltaFuerProSekunde);
        } else if (getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            Vector2 schadenPositionUntenLinks = schadenPositionUntenLinks_RechtsVonFigurBerechnen();
            schadenAusfuehren(schadenPositionUntenLinks, getSchlagenHitboxBreiteUndHohe(), getSchlagenSchaden(), true, sollGegnerTreffen, sollSpielerTreffen, deltaFuerProSekunde);
        }
    }

    public Vector2 schadenPositionUntenLinks_LinksVonFigurBerechnen(Vector2 schadenBreiteUndHoehe){
        Vector2 schadenPositionUntenLinks = new Vector2();
        schadenPositionUntenLinks.set(getPositionMitte().x - getFigurHitboxBreiteUndHoehe().x/2 - schadenBreiteUndHoehe.x -schlagenPositionModifikator.x, getPositionMitte().y-getFigurHitboxBreiteUndHoehe().y/2+schlagenPositionModifikator.y);
        /*Vector2 schadenPositionUntenLinks = getPositionMitte(); //TODO Was genau war hier falsch? (Das war das Problem mit dem falckern beim zuschlagen)
        schadenPositionUntenLinks.x = getPositionMitte().x - getFigurHitboxBreiteUndHoehe().x/2 - schadenBreiteUndHoehe.x;*/
        return schadenPositionUntenLinks;
    }
    public Vector2 schadenPositionUntenLinks_RechtsVonFigurBerechnen(){
        Vector2 schadenPositionUntenLinks = new Vector2();
        schadenPositionUntenLinks.set(getPositionMitte().x + getFigurHitboxBreiteUndHoehe().x/2 +schlagenPositionModifikator.x, getPositionMitte().y-getFigurHitboxBreiteUndHoehe().y/2+schlagenPositionModifikator.y);
        /*        getPositionMitte();
        schadenPositionUntenLinks.x = getPositionMitte().x + getFigurHitboxBreiteUndHoehe().x/2;*/
        return schadenPositionUntenLinks;
    }

    public void schadenAusfuehren(Vector2 schadenPositionUntenLinks, Vector2 schadenHitboxBreiteUndHoehe, float schaden, boolean kannGeblockWerden, boolean sollGegnerTreffen, boolean sollSpielerTreffen, float... deltaFuerProSekunde){
        float delta = deltaFuerProSekunde.length > 0 ? deltaFuerProSekunde[0] : 1;

        Rectangle schadenHitbox = erstelleHitboxUntenLinks(schadenPositionUntenLinks, schadenHitboxBreiteUndHoehe);
        Rectangle figurHitbox = new Rectangle();
        if(sollGegnerTreffen) {
            for(Figur f : level.getFigurenListe()){
                figurHitbox = erstelleHitboxMitte(f.getPositionMitte(), f.getFigurHitboxBreiteUndHoehe());
                if(figurHitbox.overlaps(schadenHitbox) && (!kannGeblockWerden || !Werkzeuge.pause(f.getBlockenStartZeit()+(long)(f.getBlockenBeginnenAnimationsDauer()/ MathUtils.nanoToSec), f.getBlockenDauer()-f.getBlockenBeginnenAnimationsDauer()))){
                    f.setLebenspunkte(f.getLebenspunkte()- schaden * delta) ;
                }
            }
        }
        if(sollSpielerTreffen){
            figurHitbox = erstelleHitboxMitte(getLevel().getFigurSpieler().getPositionMitte(), getLevel().getFigurSpieler().getFigurHitboxBreiteUndHoehe());
            if(figurHitbox.overlaps(schadenHitbox)){
                getLevel().getFigurSpieler().setLebenspunkte(getLevel().getFigurSpieler().getLebenspunkte()- schaden * delta) ;
            }
        }
    }

    public Rectangle erstelleHitboxUntenLinks(Vector2 positionUntenLinks, Vector2 hitboxBreiteUndHoehe){
        return new Rectangle(positionUntenLinks.x, positionUntenLinks.y, hitboxBreiteUndHoehe.x, hitboxBreiteUndHoehe.y);
    }
    public Rectangle erstelleHitboxMitte(Vector2 positionMitte, Vector2 hitboxBreiteUndHoehe){
        return new Rectangle(positionMitte.x-hitboxBreiteUndHoehe.x/2, positionMitte.y-getFigurHitboxBreiteUndHoehe().y/2, hitboxBreiteUndHoehe.x, hitboxBreiteUndHoehe.y);
    }






    public Vector2 getPositionMitte() {
        return positionMitte;
    }

    public void setPositionMitte(Vector2 positionMitte) {
        this.positionMitte = positionMitte;
    }

    public Vector2 getLetzterFramePosition() {
        return letzterFramePosition;
    }

    public void setLetzterFramePosition(Vector2 letzterFramePosition) {
        this.letzterFramePosition = letzterFramePosition;
    }

    public Vector2 getPositionRendern() {
        return positionRendern;
    }

    public void setPositionRendern(Vector2 positionRendern) {
        this.positionRendern = positionRendern;
    }

    public Vector2 getPositionMitteModifikator() {
        return positionMitteModifikator;
    }

    public void setPositionMitteModifikator(Vector2 positionMitteModifikator) {
        this.positionMitteModifikator = positionMitteModifikator;
    }

    public Vector2 getPositionRenderModifikator() {
        return positionRenderModifikator;
    }

    public void setPositionRenderModifikator(Vector2 positionRenderModifikator) {
        this.positionRenderModifikator = positionRenderModifikator;
    }

    public Vector2 getFigurHitboxBreiteUndHoehe() {
        return figurHitboxBreiteUndHoehe;
    }

    public void setFigurHitboxBreiteUndHoehe(Vector2 figurHitboxBreiteUndHoehe) {
        this.figurHitboxBreiteUndHoehe = figurHitboxBreiteUndHoehe;
    }

    public Vector2 getSchlagenHitboxBreiteUndHohe() {
        return schlagenHitboxBreiteUndHohe;
    }

    public void setSchlagenHitboxBreiteUndHohe(Vector2 schlagenHitboxBreiteUndHohe) {
        this.schlagenHitboxBreiteUndHohe = schlagenHitboxBreiteUndHohe;
    }

    public float getSchlagenAnimationsdauer() {
        return schlagenAnimationsdauer;
    }

    public void setSchlagenAnimationsdauer(float schlagenAnimationsdauer) {
        this.schlagenAnimationsdauer = schlagenAnimationsdauer;
    }

    public Enums.EnumLaufen getEnumLaufen() {
        return enumLaufen;
    }

    public void setEnumLaufen(Enums.EnumLaufen enumLaufen) {
        this.enumLaufen = enumLaufen;
    }

    public Enums.EnumRichtung getEnumRichtung() {
        return enumRichtung;
    }

    public void setEnumRichtung(Enums.EnumRichtung enumRichtung) {
        this.enumRichtung = enumRichtung;
    }

    public Enums.EnumAngreifen getEnumAngreifen() {
        return enumAngreifen;
    }

    public void setEnumAngreifen(Enums.EnumAngreifen enumAngreifen) {
        this.enumAngreifen = enumAngreifen;
    }

    public Enums.EnumSpringen getEnumSpringen() {
        return enumSpringen;
    }

    public void setEnumSpringen(Enums.EnumSpringen enumSpringen) {
        this.enumSpringen = enumSpringen;
    }

    public long getLaufenStartZeit() {
        return laufenStartZeit;
    }

    public void setLaufenStartZeit(long laufenStartZeit) {
        this.laufenStartZeit = laufenStartZeit;
    }

    public long getSchlagenStartZeit() {
        return schlagenStartZeit;
    }

    public void setSchlagenStartZeit(long schlagenStartZeit) {
        this.schlagenStartZeit = schlagenStartZeit;
    }

    public long getLetzteAktionZeit() {
        return letzteAktionZeit;
    }

    public void setLetzteAktionZeit(long letzteAktionZeit) {
        this.letzteAktionZeit = letzteAktionZeit;
    }

    public long getAktionBeendenZeit() {
        return aktionBeendenZeit;
    }

    public void setAktionBeendenZeit(long aktionBeendenZeit) {
        this.aktionBeendenZeit = aktionBeendenZeit;
    }

    public boolean getAktionBeenden() {
        return aktionBeenden;
    }

    public void setAktionBeenden(boolean aktionBeenden) {
        this.aktionBeenden = aktionBeenden;
    }

    public boolean getMoechteSpringen() {
        return moechteSpringen;
    }

    public void setMoechteSpringen(boolean moechteSpringen) {
        this.moechteSpringen = moechteSpringen;
    }

    public long getPauseStartzeitVonUpdate() {
        return pauseStartzeitVonUpdate;
    }

    public void setPauseStartzeitVonUpdate(long pauseStartzeitVonUpdate) {
        this.pauseStartzeitVonUpdate = pauseStartzeitVonUpdate;
    }

    public float getPauseDauerVonUpdate() {
        return pauseDauerVonUpdate;
    }

    public void setPauseDauerVonUpdate(float pauseDauerVonUpdate) {
        this.pauseDauerVonUpdate = pauseDauerVonUpdate;
    }

    public long getPauseSchlagenStartzeit() {
        return pauseSchlagenStartzeit;
    }

    public void setPauseSchlagenStartzeit(long pauseSchlagenStartzeit) {
        this.pauseSchlagenStartzeit = pauseSchlagenStartzeit;
    }

    public float getPauseSchlagenDauer() {
        return pauseSchlagenDauer;
    }

    public void setPauseSchlagenDauer(float pauseSchlagenDauer) {
        this.pauseSchlagenDauer = pauseSchlagenDauer;
    }

    public float getSchlagenSchadenVerzoegerung() {
        return schlagenSchadenVerzoegerung;
    }

    public void setSchlagenSchadenVerzoegerung(float schlagenSchadenVerzoegerung) {
        this.schlagenSchadenVerzoegerung = schlagenSchadenVerzoegerung;
    }

    public int getSchlagID() {
        return schlagID;
    }

    public void setSchlagID(int schlagID) {
        this.schlagID = schlagID;
    }

    public boolean getSchlagenBereitsGeschlagen() {
        return schlagenBereitsGeschlagen;
    }

    public void setSchlagenBereitsGeschlagen(boolean schlagenBereitsGeschlagen) {
        this.schlagenBereitsGeschlagen = schlagenBereitsGeschlagen;
    }

    public boolean getSchlagenNachDiesemSchlagBeenden() {
        return schlagenNachDiesemSchlagBeenden;
    }

    public void setSchlagenNachDiesemSchlagBeenden(boolean schlagenNachDiesemSchlagBeenden) {
        this.schlagenNachDiesemSchlagBeenden = schlagenNachDiesemSchlagBeenden;
    }

    public float getLaufenGeschwindigkeit() {
        return laufenGeschwindigkeit;
    }

    public void setLaufenGeschwindigkeit(float laufenGeschwindigkeit) {
        this.laufenGeschwindigkeit = laufenGeschwindigkeit;
    }

    public Vector2 getBewegungsgeschwindigkeit() {
        return bewegungsgeschwindigkeit;
    }

    public void setBewegungsgeschwindigkeit(Vector2 bewegungsgeschwindigkeit) {
        this.bewegungsgeschwindigkeit = bewegungsgeschwindigkeit;
    }

    public float getGewicht() {
        return gewicht;
    }

    public void setGewicht(float gewicht) {
        this.gewicht = gewicht;
    }

    public float getSprungkraft() {
        return sprungkraft;
    }

    public void setSprungkraft(float sprungkraft) {
        this.sprungkraft = sprungkraft;
    }

    public float getLebenspunkte() {
        return lebenspunkte;
    }

    public void setLebenspunkte(float lebenspunkte) {
        this.lebenspunkte = lebenspunkte;
    }

    public float getSchlagenSchaden() {
        return schlagenSchaden;
    }

    public void setSchlagenSchaden(float schlagenSchaden) {
        this.schlagenSchaden = schlagenSchaden;
    }

    public float getZeitBisIdle() {
        return zeitBisIdle;
    }

    public void setZeitBisIdle(float zeitBisIdle) {
        this.zeitBisIdle = zeitBisIdle;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public float getSchiessenAnimationsdauer() {
        return schiessenAnimationsdauer;
    }

    public void setSchiessenAnimationsdauer(float schiessenAnimationsdauer) {
        this.schiessenAnimationsdauer = schiessenAnimationsdauer;
    }

    public Vector2 getPositionSchiessen() {
        return positionSchiessen;
    }

    public void setPositionSchiessen(Vector2 positionSchiessen) {
        this.positionSchiessen = positionSchiessen;
    }

    public boolean getSchiessenBereitsGeschossen() {
        return schiessenBereitsGeschossen;
    }

    public void setSchiessenBereitsGeschossen(boolean schiessenBereitsGeschossen) {
        this.schiessenBereitsGeschossen = schiessenBereitsGeschossen;
    }

    public float getSchiessenSchadenVerzoegerung() {
        return schiessenSchadenVerzoegerung;
    }

    public void setSchiessenSchadenVerzoegerung(float schiessenSchadenVerzoegerung) {
        this.schiessenSchadenVerzoegerung = schiessenSchadenVerzoegerung;
    }

    public long getBlockenStartZeit() {
        return blockenStartZeit;
    }

    public void setBlockenStartZeit(long blockenStartZeit) {
        this.blockenStartZeit = blockenStartZeit;
    }

    public float getBlockenBeginnenAnimationsDauer() {
        return blockenBeginnenAnimationsDauer;
    }

    public void setBlockenBeginnenAnimationsDauer(float blockenBeginnenAnimationsDauer) {
        this.blockenBeginnenAnimationsDauer = blockenBeginnenAnimationsDauer;
    }

    public float getBlockenBeendenAnimationsDauer() {
        return blockenBeendenAnimationsDauer;
    }

    public void setBlockenBeendenAnimationsDauer(float blockenBeendenAnimationsDauer) {
        this.blockenBeendenAnimationsDauer = blockenBeendenAnimationsDauer;
    }

    public float getBlockenDauer() {
        return blockenDauer;
    }

    public void setBlockenDauer(float blockenDauer) {
        this.blockenDauer = blockenDauer;
    }

    public Vector2 getSchlagenPositionModifikator() {
        return schlagenPositionModifikator;
    }

    public void setSchlagenPositionModifikator(Vector2 schlagenPositionModifikator) {
        this.schlagenPositionModifikator = schlagenPositionModifikator;
    }

    public boolean getLetzterFrameGegnerKollisionX() {
        return letzterFrameGegnerKollisionX;
    }

    public void setLetzterFrameGegnerKollisionX(boolean letzterFrameGegnerKollisionX) {
        this.letzterFrameGegnerKollisionX = letzterFrameGegnerKollisionX;
    }

    public float getMaxLebenspunkte() {
        return maxLebenspunkte;
    }

    public void setMaxLebenspunkte(float maxLebenspunkte) {
        this.maxLebenspunkte = maxLebenspunkte;
    }
}
