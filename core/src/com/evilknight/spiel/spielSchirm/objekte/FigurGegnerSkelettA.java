package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 07.11.2017.
 */

public class FigurGegnerSkelettA extends FigurGegner{

    public FigurGegnerSkelettA(Vector2 position, Level level) {
        super(position, level);

        setFigurHitboxBreiteUndHoehe(KonstantenAssets.SKELETT_A_FIGUR_HITBOX_BREITE_UND_HOEHE);

        setPositionRenderModifikator(KonstantenAssets.SKELETT_A_POSITIONS_RENDER_MODIFIKATOR);
        setPositionRendern(new Vector2(position.x- KonstantenAssets.SKELETT_A_POSITIONS_RENDER_MODIFIKATOR.x, position.y-KonstantenAssets.SKELETT_A_POSITIONS_RENDER_MODIFIKATOR.y));

        setSchlagenHitboxBreiteUndHohe(KonstantenAssets.SKELETT_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE);
        setSchlagenAnimationsdauer(Konstanten.SKELETT_A_SCHLAGEN_ANIMATIONSDAUER);
        setKannBlocken(true);
        setBlockenDauer((long)(Konstanten.SKELETT_A_BLOCKEN_DAUER));
        setBlockenBeginnenAnimationsDauer((long)Konstanten.SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONDAUER);
        setBlockenBeendenAnimationsDauer((long)Konstanten.SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONDAUER);

        setSichtweite(Konstanten.SKELETT_A_SICHTWEITE);
        setLaufenGeschwindigkeit(Konstanten.SKELETT_A_BEWEGUNGSGESCHWINDIGKEIT);
        setSprungkraft(Konstanten.SKELETT_A_SPRUNGKRAFT);
        setLebenspunkte(Konstanten.SKELETT_A_LEBENSPUNKTE);
        setGewicht(1);

        setSchlagenSchaden(Konstanten.SKELETT_A_SCHLAGEN_SCHADEN);
        setSchlagenSchadenVerzoegerung(Konstanten.SKELETT_A_SCHLAGEN_VERZOEGERUNG);
    }

    public void update(float delta){
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch spriteBatch){
        TextureRegion region = Assets.instance.ritterSiAssets.mitteStehenAnimation.getKeyFrame(TimeUtils.nanoTime());
        float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());

        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN && getEnumRichtung() != Enums.EnumRichtung.MITTE){
            region = Assets.instance.skelettAAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.ANGREIFEN && /* enumLaufen == Enums.EnumLaufen.NICHT_LAUFEND && */ getEnumRichtung() != Enums.EnumRichtung.MITTE) {
            float vergangeneSchlagenZeit = Werkzeuge.secondsSince(getSchlagenStartZeit());
            region = Assets.instance.skelettAAssets.linksSchlagenAnimation.getKeyFrame(vergangeneSchlagenZeit);
        }else if(getEnumAngreifen() == Enums.EnumAngreifen.BLOCKEN) {
            if (Werkzeuge.pause(getBlockenStartZeit(), getBlockenBeginnenAnimationsDauer())) {  //Schild in Position bringen
                float vergangeneBlockenZeit = Werkzeuge.secondsSince(getBlockenStartZeit());
                region = Assets.instance.skelettAAssets.linksBlockenBeginnenAnimation.getKeyFrame(vergangeneBlockenZeit);
            } else if (Werkzeuge.pause(getBlockenStartZeit(), getBlockenDauer() - Konstanten.SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONDAUER)) {
                region = Assets.instance.skelettAAssets.linksBlocken;
            } else {
                float vergangeneBlockenZeit = Werkzeuge.secondsSince(getBlockenStartZeit() + (long)(getBlockenDauer()) - (long)getBlockenBeendenAnimationsDauer());
                region = Assets.instance.skelettAAssets.linksBlockenBeendenAnimation.getKeyFrame(vergangeneBlockenZeit);
            }
        }else if(getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND && getEnumAngreifen() == Enums.EnumAngreifen.NICHT_ANGREIFEN ){
            region = Assets.instance.skelettAAssets.linksStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }

        Werkzeuge.zeichneTexturRegion(spriteBatch, region, getPositionRendern(), getEnumRichtung()== Enums.EnumRichtung.RECHTS);
    }
}
