package com.evilknight.spiel.spielSchirm.objekte;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.werkzeuge.Assets;
import com.evilknight.spiel.werkzeuge.Enums;
import com.evilknight.spiel.werkzeuge.Konstanten;
import com.evilknight.spiel.werkzeuge.KonstantenAssets;
import com.evilknight.spiel.werkzeuge.Werkzeuge;

/**
 * Created by LeonPB on 29.10.2017.
 */

public class FigurGegnerZombieFranken extends FigurGegner {



    public FigurGegnerZombieFranken(Vector2 position, Level level) {
        super(position, level);
        setPositionMitteModifikator(KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_MITTE_MODIFIKATOR);
        setPositionRenderModifikator(KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR);
        setPositionMitte(new Vector2(position.x+KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_MITTE_MODIFIKATOR.x, position.y+KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_MITTE_MODIFIKATOR.y));
        setPositionRendern(new Vector2(position.x- KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR.x, position.y-KonstantenAssets.ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR.y));
        setFigurHitboxBreiteUndHoehe(Konstanten.ZOMBIE_FRANKEN_FIGUR_HITBOX_BREITE_UND_HOEHE);
        setSchlagenHitboxBreiteUndHohe(Konstanten.ZOMBIE_FRANKEN_SCHLAGEN_HITBOX_BREITE_UND_HOEHE);
        setSchlagenAnimationsdauer(Konstanten.ZOMBIE_FRANKEN_SCHLAGEN_ANIMATIONSDAUER);

        setSichtweite(Konstanten.ZOMBIE_FRANKEN_SICHTWEITE);
        setLaufenGeschwindigkeit(Konstanten.ZOMBIE_FRANKEN_BEWEGUNGSGESCHWINDIGKEIT);
        setLebenspunkte(Konstanten.ZOMBIE_FRANKEN_LEBENSPUNKTE);
        setSchlagenSchaden(Konstanten.ZOMBIE_FRANKEN_SCHLAGEN_SCHADEN);
        setSchlagenSchadenVerzoegerung(Konstanten.ZOMBIE_FRANKEN_SCHLAGEN_VERZOEGERUNG);
        setGewicht(1);

    }

    public void update(float delta){
            super.update(delta);
    }

    @Override
    public void render(SpriteBatch spriteBatch){
        /*TextureRegion region = Assets.instance.ritterSAssets.rechtsStehen;*/
        TextureRegion region = Assets.instance.zombieFrankenAssets.mitteStehen;
        float vergangeneLaufenZeit = Werkzeuge.secondsSince(getLaufenStartZeit());
        if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumRichtung() == Enums.EnumRichtung.LINKS) {
            region = Assets.instance.zombieFrankenAssets.linksLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.LAUFEND && getEnumRichtung() == Enums.EnumRichtung.RECHTS) {
            region = Assets.instance.zombieFrankenAssets.rechtsLaufenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }else if(getEnumLaufen() == Enums.EnumLaufen.NICHT_LAUFEND) {
            region = Assets.instance.zombieFrankenAssets.mitteStehenAnimation.getKeyFrame(vergangeneLaufenZeit);
        }
            Werkzeuge.drawTextureRegion(spriteBatch, region, getPositionRendern());

    }
}
