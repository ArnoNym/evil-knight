package com.evilknight.spiel;

import com.badlogic.gdx.Game;
import com.evilknight.spiel.spielSchirm.SpielSchirm;

public class EvilKnightSpiel extends Game {

	@Override
	public void create(){
		setScreen(new SpielSchirm());
	}
}