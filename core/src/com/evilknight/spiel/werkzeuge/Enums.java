package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.math.MathUtils;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class Enums {
//TODO Kann man enumLaufe, Angreifen und vielleicht sogar Sprigen nicht in eines machen?
    public enum EnumLaufen { //Springen und laufen seperat damit man auch in der Luft noch die Richtung ändern kann
        LAUFEND,
        STUERMEN,
        NICHT_LAUFEND //Weil fliegen usw ja nicht stehen ist
    }
    public enum EnumRichtung {
        LINKS, RECHTS, OBEN, UNTEN, MITTE,
        NEIN //Für das überprüfen ob ein gegner gesehen wird. Erspart ein weiteres Enum und unnötige Arbeit die beiden ineinander zu übersetzen
    }

    public enum EnumAngreifen {
        ANGREIFEN, NICHT_ANGREIFEN, BLOCKEN
    }
    public enum EnumAngreifenRichtung {
        LINKS, RECHTS, MITTE
    }

    public enum EnumSpringen {
        SPRINGEND, FALLEND, BODEN,
        STUERZEND //Damit man eine zusätzliche Haltung hat bei der man keine Kontrolle mehr hat
    }

    public enum EnumSieht {
        JA_LINKS, JA_RECHTS, NEIN
    }

    public enum EnumHintergruendeFriedhofHoehe {
        MITTE, UNTEN
    }

    /*
    public enum EnumHintergruendeFriedhof {
        HF1, HF2, HF3, HF4;
        public static EnumHintergruendeFriedhof Zufaellig(){
            int z = MathUtils.random(5); //Damit die normale Fläche öfter drankommt
            if(z==1){return HF1;}
            else if(z==2){return HF2;}
            else if(z==4){return HF4;}
            else{return HF3;}
        }
    }
    public static EnumHintergruendeFriedhof Zufaellig(){
        int z = MathUtils.random(4);
        if(z==1){return EnumHintergruendeFriedhof.HF1;}
        else if(z==2){return EnumHintergruendeFriedhof.HF2;}
        else if(z==3){return EnumHintergruendeFriedhof.HF3;}
        else{return EnumHintergruendeFriedhof.HF4;}
    }
    */
}
