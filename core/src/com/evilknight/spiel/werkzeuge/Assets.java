package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class Assets implements Disposable, AssetErrorListener {

    public static final String linksLaufenAnimationArray = Assets.class.getName();
    public static final Assets instance = new Assets();

    //Hier Assetklassen von neuen Objekten einfügen
    public HudAssets hudAssets;
    public PfeilAssets pfeilAssets;
    public BogenschuetzeAAssets bogenschuetzeAAssets;
    public BauerAAssets bauerAAssets;
    public ZombieAAssets zombieAAssets;
    public ZombieFrankenAssets zombieFrankenAssets;
    public RitterSiAssets ritterSiAssets;
    public BildschirmtastenAssets bildschirmtastenAssets;
    public SkelettAAssets skelettAAssets;

    private AssetManager assetManager;

    private Assets(){
    }

    public void init(AssetManager assetManager){
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(KonstantenAssets.TEXTUREN_ATLAS, TextureAtlas.class);
        assetManager.finishLoading();

        TextureAtlas atlas = assetManager.get(KonstantenAssets.TEXTUREN_ATLAS);
        //Hier Assetklassen von neuen Objekten deklarieren
        hudAssets = new HudAssets(atlas);
        pfeilAssets = new PfeilAssets(atlas);
        bogenschuetzeAAssets = new BogenschuetzeAAssets(atlas);
        bauerAAssets = new BauerAAssets(atlas);
        zombieAAssets = new ZombieAAssets(atlas);
        zombieFrankenAssets = new ZombieFrankenAssets(atlas);
        ritterSiAssets = new RitterSiAssets(atlas);
        bildschirmtastenAssets = new BildschirmtastenAssets(atlas);
        skelettAAssets = new SkelettAAssets(atlas);
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(linksLaufenAnimationArray, "Couldn't load asset: " + asset.fileName, throwable);
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }

    public class HudAssets {
        public final AtlasRegion herz01von10;
        public final AtlasRegion herz02von10;
        public final AtlasRegion herz03von10;
        public final AtlasRegion herz04von10;
        public final AtlasRegion herz05von10;
        public final AtlasRegion herz06von10;
        public final AtlasRegion herz07von10;
        public final AtlasRegion herz08von10;
        public final AtlasRegion herz09von10;
        public final AtlasRegion herz10von10;

        public HudAssets(TextureAtlas atlas) {
            herz01von10 = atlas.findRegion("lebensbalkenSpieler10");
            herz02von10 = atlas.findRegion("lebensbalkenSpieler9");
            herz03von10 = atlas.findRegion("lebensbalkenSpieler8");
            herz04von10 = atlas.findRegion("lebensbalkenSpieler7");
            herz05von10 = atlas.findRegion("lebensbalkenSpieler6");
            herz06von10 = atlas.findRegion("lebensbalkenSpieler5");
            herz07von10 = atlas.findRegion("lebensbalkenSpieler4");
            herz08von10 = atlas.findRegion("lebensbalkenSpieler3");
            herz09von10 = atlas.findRegion("lebensbalkenSpieler2");
            herz10von10 = atlas.findRegion("lebensbalkenSpieler1");
        }
    }

    public class BauerAAssets {
        public final Animation linksStehenAnimation;
        public final Animation linksLaufenAnimation;
        public final Animation linksSchlagenAnimation;
        public final Animation linksStuermenAnimation;
        public final Animation linksSterbenAnimation;

        public BauerAAssets(TextureAtlas atlas) {
            Array<AtlasRegion>linksStehenAnimationArray = new Array<AtlasRegion>();
            linksStehenAnimationArray.add(atlas.findRegion("bauerA1"));
            linksStehenAnimationArray.add(atlas.findRegion("bauerA2"));
            linksStehenAnimationArray.add(atlas.findRegion("bauerA3"));
            linksStehenAnimation = new Animation(KonstantenAssets.BAUER_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, linksStehenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA4"));
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA5"));
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA6"));
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA7"));
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA8"));
            linksLaufenAnimationArray.add(atlas.findRegion("bauerA9"));
            linksLaufenAnimation = new Animation(KonstantenAssets.BAUER_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSchlagenAnimationArray = new Array<AtlasRegion>();
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA10"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA11"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA12"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA13"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA14"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA15"));
            linksSchlagenAnimationArray.add(atlas.findRegion("bauerA16"));
            linksSchlagenAnimation = new Animation(KonstantenAssets.BAUER_A_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES, linksSchlagenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksStuermenAnimationArray = new Array<AtlasRegion>();
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA17"));
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA18"));
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA19"));
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA20"));
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA21"));
            linksStuermenAnimationArray.add(atlas.findRegion("bauerA22"));
            linksStuermenAnimation = new Animation(KonstantenAssets.BAUER_A_STUERMEN_ANIMATIONSDAUER_EINES_FRAMES, linksStuermenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSterbenAnimationArray = new Array<AtlasRegion>();
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA23"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA24"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA25"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA26"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA27"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA28"));
            linksSterbenAnimationArray.add(atlas.findRegion("bauerA29"));
            linksSterbenAnimation = new Animation(KonstantenAssets.BAUER_A_STERBEN_ANIMATIONSDAUER_EINES_FRAMES, linksSterbenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class ZombieAAssets {
        public final AtlasRegion linksStehen;

        public final Animation linksLaufenAnimation;

        public ZombieAAssets(TextureAtlas atlas) {
            linksStehen = atlas.findRegion(KonstantenAssets.KERRIGAN);
            //linksStehen = atlas.findRegion(KonstantenAssets.ZOMBIE_A_LINKS_STEHEN_0);

            Array<AtlasRegion> linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_A_LINKS_LAUFEN_0));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_A_LINKS_LAUFEN_1));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_A_LINKS_LAUFEN_2));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_A_LINKS_LAUFEN_3));
            linksLaufenAnimation = new Animation(Konstanten.ZOMBIE_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class RitterSiAssets {
        public final Animation mitteStehenAnimation;
        public final AtlasRegion linksStehen;
        public final AtlasRegion rechtsStehen;
        public final Animation linksSchlagenAnimation;
        public final Animation linksLaufenAnimation;
        public final Animation rechtsLaufenAnimation;

        public RitterSiAssets(TextureAtlas atlas) {
            Array<AtlasRegion> mitteStehenAnimationArray = new Array<AtlasRegion>();
            mitteStehenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_MITTE_STEHEN_1));
            mitteStehenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_MITTE_STEHEN_2));
            mitteStehenAnimation = new Animation(KonstantenAssets.RITTER_Si_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, mitteStehenAnimationArray, Animation.PlayMode.LOOP);

            linksStehen = atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_STEHEN_1);
            rechtsStehen = atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_STEHEN_1);

            Array<AtlasRegion> linksSchlagenAnimationArray = new Array<AtlasRegion>();
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_1));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_2));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_3));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_4));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_5));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_6));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_7));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_8));
            linksSchlagenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_SCHLAGEN_9));
            linksSchlagenAnimation = new Animation(KonstantenAssets.RITTER_Si_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES, linksSchlagenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion> linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_LAUFEN_1));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_LAUFEN_2));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_LAUFEN_3));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_LAUFEN_4));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_LINKS_LAUFEN_5));
            linksLaufenAnimation = new Animation(Konstanten.RITTER_Si_LAUFEN_ANIMATIONSDAUER, linksLaufenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion> rechtsLaufenAnimationArray = new Array<AtlasRegion>();
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_LAUFEN_1));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_LAUFEN_2));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_LAUFEN_3));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_LAUFEN_4));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.RITTER_Si_RECHTS_LAUFEN_5));
            rechtsLaufenAnimation = new Animation(Konstanten.RITTER_Si_LAUFEN_ANIMATIONSDAUER, rechtsLaufenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class ZombieFrankenAssets {
        public final AtlasRegion mitteStehen;

        public final Animation mitteStehenAnimation;
        public final Animation linksLaufenAnimation;
        public final Animation rechtsLaufenAnimation;

        public ZombieFrankenAssets(TextureAtlas atlas) {
            mitteStehen = atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_1);

            Array<AtlasRegion>mitteStehenAnimationArray = new Array<AtlasRegion>();
            mitteStehenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_1));
            mitteStehenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_2));
            mitteStehenAnimation = new Animation(Konstanten.ZOMBIE_FRANKEN_MITTE_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, mitteStehenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_LINKS_LAUFEN_1));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_LINKS_LAUFEN_2));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_LINKS_LAUFEN_3));
            linksLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_LINKS_LAUFEN_4));
            linksLaufenAnimation = new Animation(Konstanten.ZOMBIE_FRANKEN_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>rechtsLaufenAnimationArray = new Array<AtlasRegion>();
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_RECHTS_LAUFEN_1));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_RECHTS_LAUFEN_2));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_RECHTS_LAUFEN_3));
            rechtsLaufenAnimationArray.add(atlas.findRegion(KonstantenAssets.ZOMBIE_FRANKEN_RECHTS_LAUFEN_4));
            rechtsLaufenAnimation = new Animation(Konstanten.ZOMBIE_FRANKEN_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, rechtsLaufenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class SkelettAAssets {
        public final Animation linksStehenAnimation;
        public final Animation linksLaufenAnimation;
        public final Animation linksSchlagenAnimation;
        public final Animation linksBlockenBeginnenAnimation;
        public final AtlasRegion linksBlocken;
        public final Animation linksBlockenBeendenAnimation;

        public SkelettAAssets(TextureAtlas atlas) {
            Array<AtlasRegion>linksStehenAnimationArray = new Array<AtlasRegion>();
            linksStehenAnimationArray.add(atlas.findRegion("skelettA2"));
            linksStehenAnimationArray.add(atlas.findRegion("skelettA3"));
            linksStehenAnimationArray.add(atlas.findRegion("skelettA4"));
            linksStehenAnimationArray.add(atlas.findRegion("skelettA5"));
            linksStehenAnimation = new Animation(KonstantenAssets.SKELETT_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, linksStehenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA6"));
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA7"));
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA8"));
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA9"));
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA10"));
            linksLaufenAnimationArray.add(atlas.findRegion("skelettA11"));
            linksLaufenAnimation = new Animation(KonstantenAssets.SKELETT_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSchlagenAnimationArray = new Array<AtlasRegion>();
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA12"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA13"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA14"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA15"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA16"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA17"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA18"));
            linksSchlagenAnimationArray.add(atlas.findRegion("skelettA19"));
            linksSchlagenAnimation = new Animation(KonstantenAssets.SKELETT_A_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES, linksSchlagenAnimationArray, Animation.PlayMode.LOOP);

            //BLOCKEN
            Array<AtlasRegion>linksBlockenBeginnenAnimationArray = new Array<AtlasRegion>();
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA20"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA21"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA22"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA23"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA24"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA25"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA26"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA27"));
            linksBlockenBeginnenAnimationArray.add(atlas.findRegion("skelettA28"));
            linksBlockenBeginnenAnimation = new Animation(KonstantenAssets.SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONSDAUER_EINES_FRAMES, linksBlockenBeginnenAnimationArray, Animation.PlayMode.LOOP);
            //
            linksBlocken = atlas.findRegion("skelettA28");
            //
            Array<AtlasRegion>linksBlockenBeendenAnimationArray = new Array<AtlasRegion>();
            linksBlockenBeendenAnimationArray.add(atlas.findRegion("skelettA29"));
            linksBlockenBeendenAnimationArray.add(atlas.findRegion("skelettA30"));
            linksBlockenBeendenAnimation = new Animation(KonstantenAssets.SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONSDAUER_EINES_FRAMES, linksBlockenBeendenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class BogenschuetzeAAssets {
        public final Animation linksStehenAnimation;
        public final Animation linksLaufenAnimation;
        public final Animation linksSchiessenAnimation;

        public BogenschuetzeAAssets(TextureAtlas atlas) {
            Array<AtlasRegion>linksStehenAnimationArray = new Array<AtlasRegion>();
            linksStehenAnimationArray.add(atlas.findRegion("bogenschuetzeA1"));
            linksStehenAnimationArray.add(atlas.findRegion("bogenschuetzeA2"));
            linksStehenAnimationArray.add(atlas.findRegion("bogenschuetzeA3"));
            linksStehenAnimationArray.add(atlas.findRegion("bogenschuetzeA4"));
            linksStehenAnimation = new Animation(KonstantenAssets.BOGENSCHUETZE_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES, linksStehenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksSchiessenAnimationArray = new Array<AtlasRegion>();
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA5"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA6"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA7"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA8"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA9"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA10"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA11"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA12"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA13"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA14"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA15"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA16"));
            linksSchiessenAnimationArray.add(atlas.findRegion("bogenschuetzeA17"));
            linksSchiessenAnimation = new Animation(KonstantenAssets.BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER_EINES_FRAMES, linksSchiessenAnimationArray, Animation.PlayMode.LOOP);

            Array<AtlasRegion>linksLaufenAnimationArray = new Array<AtlasRegion>();
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA18"));
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA19"));
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA20"));
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA21"));
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA22"));
            linksLaufenAnimationArray.add(atlas.findRegion("bogenschuetzeA23"));
            linksLaufenAnimation = new Animation(KonstantenAssets.BOGENSCHUETZE_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES, linksLaufenAnimationArray, Animation.PlayMode.LOOP);
        }
    }

    public class PfeilAssets {
        public final AtlasRegion pfeil;
        public PfeilAssets(TextureAtlas atlas) {
            pfeil = atlas.findRegion("pfeilA");
        }
    }

    public class BildschirmtastenAssets {
        public final AtlasRegion links;
        public final AtlasRegion rechts;
        public final AtlasRegion oben;
        public final AtlasRegion unten;

        public BildschirmtastenAssets(TextureAtlas atlas) {
            links = atlas.findRegion(KonstantenAssets.BILDSCHIRMTASTEN_1);
            rechts = atlas.findRegion(KonstantenAssets.BILDSCHIRMTASTEN_2);
            oben = atlas.findRegion(KonstantenAssets.BILDSCHIRMTASTEN_3);
            unten = atlas.findRegion(KonstantenAssets.BILDSCHIRMTASTEN_4);
        }
    }
}
