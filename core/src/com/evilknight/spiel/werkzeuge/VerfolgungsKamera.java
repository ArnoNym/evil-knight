package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.evilknight.spiel.spielSchirm.Level;
import com.evilknight.spiel.spielSchirm.objekte.FigurSpieler;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class VerfolgungsKamera {

    public Camera kamera;
    public FigurSpieler ziel;
    public boolean verfolgt;
    public Level level;

    public VerfolgungsKamera(Level level){
        this.level = level;
        //ziel = level.getFigurSpieler();
        verfolgt = true;
    }

    public void update(float delta){
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            verfolgt = !verfolgt;
        }

        if (verfolgt) {
            kamera.position.x = ziel.getPositionMitte().x;
            kamera.position.y = ziel.getPositionMitte().y + ziel.getFigurHitboxBreiteUndHoehe().y/2;
            //kamera.translate(ziel.getPositionMitte().x, ziel.getPositionMitte().y + ziel.getFigurHitboxBreiteUndHoehe().y/2);

            /* TODO Je nach Richtung die Kamera weiter bewegen. Ist das eine gute Idee? Auf jedenfall erst mit Hintegrund usw einfügen weil das sonst komich aussieht
            if(level.getFigurSpielerRitter().getEnumRichtung() == Enums.EnumRichtung.LINKS){
                kamera.position.x -= Konstanten.VERFOLGUNGS_KAMERA_GUCKRICHTUNG_ERWEITERTES_SICHTFELD;
            }else if(level.getFigurSpielerRitter().getEnumRichtung() == Enums.EnumRichtung.RECHTS){
                kamera.position.x += Konstanten.VERFOLGUNGS_KAMERA_GUCKRICHTUNG_ERWEITERTES_SICHTFELD;
            } */
            if(kamera.position.y - kamera.viewportHeight/2 < Konstanten.VERFOLGUNGS_KAMERA_VERSCHIEBUNG){
                kamera.position.y = kamera.viewportHeight/2 + Konstanten.VERFOLGUNGS_KAMERA_VERSCHIEBUNG;
            }
            if(kamera.position.x - kamera.viewportWidth/2 < Konstanten.VERFOLGUNGS_KAMERA_VERSCHIEBUNG){
                kamera.position.x = kamera.viewportWidth/2 + Konstanten.VERFOLGUNGS_KAMERA_VERSCHIEBUNG;
            }
        } else {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                kamera.position.x -= delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
                //kamera.translate(- delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                kamera.position.x += delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
                //kamera.translate(+ delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                kamera.position.y += delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
                //kamera.translate(+ delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                kamera.position.y -= delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT;
                //kamera.translate(- delta * Konstanten.VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT, 0);
            }
        }
    }
}
