package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class KonstantenAssets {
    public static final String TEXTUREN_ATLAS = "images/evilknight.pack.atlas";

    //TILED
    //public static final String TILESET_NAME = "testTiled.tmx";
    //public static final String TILESET_NAME = "tetstestetsTiled.tmx";
    public static final String TILESET_NAME = "level_1_tiledmap_1.tmx";
    //public static final int TILED_KOLLISIONLAYER_INDEX = 0;
    public static final String TILED_KOLLISIONLAYER_INDEX = "Objekte";
    public static final String TILED_SPAWNEN_INDEX = "Spawnen";
    public static final String BLOCK_RECHTS = "block_rechts";
    public static final String BLOCK_LINKS = "block_links";
    public static final String BLOCK_OBEN = "block_oben"; //Wenn man von oben drauf springt
    public static final String BLOCK_UNTEN = "block_unten"; //Wenn man von unten gegen springt

    //Bildschirmtasten
    public static final String BILDSCHIRMTASTEN_1 = "bildschirmtasten1";
    public static final String BILDSCHIRMTASTEN_2 = "bildschirmtasten2";
    public static final String BILDSCHIRMTASTEN_3 = "bildschirmtasten3";
    public static final String BILDSCHIRMTASTEN_4 = "bildschirmtasten4";

    //ImLevelHUD
    public static final Vector2 HUD_LEBENSBALKEN_SPIELER_POSITION = new Vector2(5, 105);
    public static final Vector2 HUD_HERZ_HITBOX_BREITE_UND_HOEHE = new Vector2(10, 10);
    public static final float HUD_KEINEN_GEGNER_GESEHEN_PAUSE_DAUER = 3f;


    //RitterSi
    public static final Vector2 RITTER_Si_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(10*2, 32*2);
    public static final Vector2 RITTER_Si_SCHLAGEN_HITBOX_BREITE_UND_HOEHE = new Vector2(30*2, 50*2);

    public static final Vector2 RITTER_Si_POSITION_RENDER_MODIFIKATOR = new Vector2(47*2, RITTER_Si_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2);

    public static final float RITTER_Si_MITTE_STEHEN_ANZAHL_BILDER = 2f; //Für das korrekte berechnen der Animationsdauer
    public static final float RITTER_Si_STEHEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.RITTER_Si_STEHEN_ANIMATIONSDAUER/KonstantenAssets.RITTER_Si_MITTE_STEHEN_ANZAHL_BILDER;
    public static final float RITTER_Si_SCHLAGEN_ANZAHL_BILDER = 9f; //Für das korrekte berechnen der Animationsdauer //Unter Werkzeuge hinzufügen um dauer der Animationen zu reseten
    public static final float RITTER_Si_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.RITTER_Si_SCHLAGEN_ANIMATIONSDAUER/KonstantenAssets.RITTER_Si_SCHLAGEN_ANZAHL_BILDER;
    public static final float RITTER_Si_LAUFEN_ANZAHL_BILDER = 5f; //Für das korrekte berechnen der Animationsdauer
    public static final float RITTER_Si_SCHLAGEN_SCHADEN_VERZOEGERUNG = Konstanten.RITTER_Si_SCHLAGEN_ANIMATIONSDAUER * 6/RITTER_Si_SCHLAGEN_ANZAHL_BILDER;//RITTER_Si_SCHLAGEN_ANIMATIONSDAUER*RITTER_Si_SCHLAGEN_SCHADEN_VERZOEGERUNG_IN_PROZENT; //Sekunden die vergehen bis der Schaden ausgeführt wird

    public static final String RITTER_Si_MITTE_STEHEN_1 = "ritterSi1";
    public static final String RITTER_Si_MITTE_STEHEN_2 = "ritterSi2";
    public static final String RITTER_Si_LINKS_STEHEN_1 = "ritterSi3";
    public static final String RITTER_Si_RECHTS_STEHEN_1 = "ritterSi4";
    public static final String RITTER_Si_LINKS_SCHLAGEN_1 = "ritterSi5";
    public static final String RITTER_Si_LINKS_SCHLAGEN_2 = "ritterSi6";
    public static final String RITTER_Si_LINKS_SCHLAGEN_3 = "ritterSi7";
    public static final String RITTER_Si_LINKS_SCHLAGEN_4 = "ritterSi8";
    public static final String RITTER_Si_LINKS_SCHLAGEN_5 = "ritterSi9";
    public static final String RITTER_Si_LINKS_SCHLAGEN_6 = "ritterSi10";
    public static final String RITTER_Si_LINKS_SCHLAGEN_7 = "ritterSi11";
    public static final String RITTER_Si_LINKS_SCHLAGEN_8 = "ritterSi12";
    public static final String RITTER_Si_LINKS_SCHLAGEN_9 = "ritterSi13";
    public static final String RITTER_Si_LINKS_LAUFEN_1 = "ritterSi23";
    public static final String RITTER_Si_LINKS_LAUFEN_2 = "ritterSi24";
    public static final String RITTER_Si_LINKS_LAUFEN_3 = "ritterSi25";
    public static final String RITTER_Si_LINKS_LAUFEN_4 = "ritterSi26";
    public static final String RITTER_Si_LINKS_LAUFEN_5 = "ritterSi27";
    public static final String RITTER_Si_RECHTS_LAUFEN_1 = "ritterSi28";
    public static final String RITTER_Si_RECHTS_LAUFEN_2 = "ritterSi29";
    public static final String RITTER_Si_RECHTS_LAUFEN_3 = "ritterSi30";
    public static final String RITTER_Si_RECHTS_LAUFEN_4 = "ritterSi31";
    public static final String RITTER_Si_RECHTS_LAUFEN_5 = "ritterSi32";

    //FigurGegnerZombieA
    public static final Vector2 ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR = new Vector2(20, 0);
    public static final Vector2 ZOMBIE_A_POSITIONS_MITTE_MODIFIKATOR = new Vector2(35-ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR.x, 18-ZOMBIE_A_POSITIONS_RENDER_MODIFIKATOR.y);

    public static final float ZOMBIE_A_LAUFEN_ANZAHL_BILDER = 4f; //Für das korrekte berechnen der Animationsdauer

    public static final String ZOMBIE_A_LINKS_STEHEN_0 = "zombieA-links-stehen-0";
    public static final String ZOMBIE_A_LINKS_LAUFEN_0 = "zombieA-links-laufen-0";
    public static final String ZOMBIE_A_LINKS_LAUFEN_1 = "zombieA-links-laufen-1";
    public static final String ZOMBIE_A_LINKS_LAUFEN_2 = "zombieA-links-laufen-2";
    public static final String ZOMBIE_A_LINKS_LAUFEN_3 = "zombieA-links-laufen-3";

    //FigurGegnerZombieFranken
    public static final Vector2 ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR = new Vector2(15, 0);
    public static final Vector2 ZOMBIE_FRANKEN_POSITIONS_MITTE_MODIFIKATOR = new Vector2(26-ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR.x, 0-ZOMBIE_FRANKEN_POSITIONS_RENDER_MODIFIKATOR.y);

    public static final float ZOMBIE_FRANKEN_SCHLAGEN_ANZAHL_BILDER = 1f; //TODO
    public static final float ZOMBIE_FRANKEN_MITTE_STEHEN_ANZAHL_BILDER = 2f;
    public static final float ZOMBIE_FRANKEN_LAUFEN_ANZAHL_BILDER = 4f;

    public static final String ZOMBIE_FRANKEN_MITTE_STEHEN_1 = "zombieFranken1";
    public static final String ZOMBIE_FRANKEN_MITTE_STEHEN_2 = "zombieFranken2";
    public static final String ZOMBIE_FRANKEN_LINKS_STEHEN_1 = "zombieFranken3";
    public static final String ZOMBIE_FRANKEN_RECHTS_STEHEN_1 = "zombieFranken4";
    public static final String ZOMBIE_FRANKEN_LINKS_LAUFEN_1 = "zombieFranken5";
    public static final String ZOMBIE_FRANKEN_LINKS_LAUFEN_2 = "zombieFranken6";
    public static final String ZOMBIE_FRANKEN_LINKS_LAUFEN_3 = "zombieFranken7";
    public static final String ZOMBIE_FRANKEN_LINKS_LAUFEN_4 = "zombieFranken8";
    public static final String ZOMBIE_FRANKEN_RECHTS_LAUFEN_1 = "zombieFranken9";
    public static final String ZOMBIE_FRANKEN_RECHTS_LAUFEN_2 = "zombieFranken10";
    public static final String ZOMBIE_FRANKEN_RECHTS_LAUFEN_3 = "zombieFranken11";
    public static final String ZOMBIE_FRANKEN_RECHTS_LAUFEN_4 = "zombieFranken12";

    //FigurGegnerSkelettA
    public static final Vector2 SKELETT_A_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(24, 67);
    public static final Vector2 SKELETT_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE = new Vector2(52, 84); //TODO

    public static final Vector2 SKELETT_A_POSITIONS_RENDER_MODIFIKATOR = new Vector2(80, SKELETT_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2);

    public static final float SKELETT_A_SCHLAGEN_ANZAHL_BILDER = 8f; //TODO
    public static final float SKELETT_A_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.SKELETT_A_SCHLAGEN_ANIMATIONSDAUER/KonstantenAssets.SKELETT_A_SCHLAGEN_ANZAHL_BILDER;
    public static final float SKELETT_A_STEHEN_ANZAHL_BILDER = 4f;
    public static final float SKELETT_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.SKELETT_A_STEHEN_ANIMATIONSDAUER/KonstantenAssets.SKELETT_A_STEHEN_ANZAHL_BILDER;
    public static final float SKELETT_A_LAUFEN_ANZAHL_BILDER = 6f;
    public static final float SKELETT_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.SKELETT_A_LAUFEN_ANIMATIONDAUER/KonstantenAssets.SKELETT_A_LAUFEN_ANZAHL_BILDER;
    public static final float SKELETT_A_BLOCKEN_BEGINNEN_ANZAHL_BILDER = 8f; //Die Anzahl an Bildern die benötigt werden bis das Schild in Position ist
    public static final float SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONDAUER/KonstantenAssets.SKELETT_A_BLOCKEN_BEGINNEN_ANZAHL_BILDER;
    public static final float SKELETT_A_BLOCKEN_BEENDEN_ANZAHL_BILDER = 2f; //Die Anzahl an Bildern die benötigt werden bis das Schild in Position ist
    public static final float SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONDAUER/KonstantenAssets.SKELETT_A_BLOCKEN_BEENDEN_ANZAHL_BILDER;

    //FigurGegnerBauerA
    public static final Vector2 BAUER_A_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(27, 75);
    public static final Vector2 BAUER_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE = new Vector2(43, 12);
    public static final Vector2 BAUER_A_SCHLAGEN_POSITION_MODIFIKATOR = new Vector2(0, 31);

    public static final Vector2 BAUER_A_POSITIONS_RENDER_MODIFIKATOR = new Vector2(72, BAUER_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2);

    public static final float BAUER_A_SCHLAGEN_ANZAHL_BILDER = 7f;
    public static final float BAUER_A_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BAUER_A_SCHLAGEN_ANIMATIONSDAUER/BAUER_A_SCHLAGEN_ANZAHL_BILDER;
    public static final float BAUER_A_STEHEN_ANZAHL_BILDER = 3f;
    public static final float BAUER_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BAUER_A_STEHEN_ANIMATIONSDAUER/BAUER_A_STEHEN_ANZAHL_BILDER;
    public static final float BAUER_A_LAUFEN_ANZAHL_BILDER = 6f;
    public static final float BAUER_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BAUER_A_LAUFEN_ANIMATIONDAUER/BAUER_A_LAUFEN_ANZAHL_BILDER;
    public static final float BAUER_A_STUERMEN_ANZAHL_BILDER = 6f;
    public static final float BAUER_A_STUERMEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BAUER_A_STUERMEN_ANIMATIONDAUER/BAUER_A_STUERMEN_ANZAHL_BILDER;
    public static final float BAUER_A_STERBEN_ANZAHL_BILDER = 7f;
    public static final float BAUER_A_STERBEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BAUER_A_STERBEN_ANIMATIONDAUER/BAUER_A_STERBEN_ANZAHL_BILDER;

    //FigurGegnerBogenschuetzeA
    public static final Vector2 BOGENSCHUETZE_A_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(22, 72);

    public static final Vector2 BOGENSCHUETZE_A_POSITIONS_RENDER_MODIFIKATOR = new Vector2(56, BOGENSCHUETZE_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2);
    public static final Vector2 BOGENSCHUETZE_A_POSITIONS_SCHIESSEN_MODIFIKATOR = new Vector2(30, 5);

    public static final float BOGENSCHUETZE_A_SCHIESSEN_SCHADEN_VERZOEGERUNG = 0.65f * Konstanten.BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER;  //Sekunden die vergehen bis der Schaden ausgeführt wird
    public static final float BOGENSCHUETZE_A_SCHIESSEN_ANZAHL_BILDER = 13f;
    public static final float BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER/BOGENSCHUETZE_A_SCHIESSEN_ANZAHL_BILDER;
    public static final float BOGENSCHUETZE_A_STEHEN_ANZAHL_BILDER = 4f;
    public static final float BOGENSCHUETZE_A_STEHEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BOGENSCHUETZE_A_STEHEN_ANIMATIONSDAUER/BOGENSCHUETZE_A_STEHEN_ANZAHL_BILDER;
    public static final float BOGENSCHUETZE_A_LAUFEN_ANZAHL_BILDER = 6f;
    public static final float BOGENSCHUETZE_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BOGENSCHUETZE_A_LAUFEN_ANIMATIONDAUER/BOGENSCHUETZE_A_LAUFEN_ANZAHL_BILDER;
    public static final float BOGENSCHUETZE_A_STERBEN_ANZAHL_BILDER = 7f;
    public static final float BOGENSCHUETZE_A_STERBEN_ANIMATIONSDAUER_EINES_FRAMES = Konstanten.BOGENSCHUETZE_A_STERBEN_ANIMATIONDAUER/BOGENSCHUETZE_A_STERBEN_ANZAHL_BILDER;

    //Pfeil
    public static final Vector2 PFEIL_A_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(30f, 30f);

    //Kerrigan
    public static final String KERRIGAN = "kerriganA1";



}
