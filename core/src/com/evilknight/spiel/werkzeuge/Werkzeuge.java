package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.evilknight.spiel.spielSchirm.objekte.Figur;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class Werkzeuge {

    public static float laegnsteAnimationsdauer(){ //Die Dauer der lägsten Animation auswählen
        return maximalerFloat(Konstanten.RITTER_Si_LAUFEN_ANIMATIONSDAUER * KonstantenAssets.RITTER_Si_LAUFEN_ANZAHL_BILDER,
                KonstantenAssets.RITTER_Si_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES * KonstantenAssets.RITTER_Si_SCHLAGEN_ANZAHL_BILDER,
                KonstantenAssets.RITTER_Si_STEHEN_ANIMATIONSDAUER_EINES_FRAMES * KonstantenAssets.RITTER_Si_MITTE_STEHEN_ANZAHL_BILDER,
                Konstanten.ZOMBIE_FRANKEN_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES * KonstantenAssets.ZOMBIE_FRANKEN_LAUFEN_ANZAHL_BILDER,
                Konstanten.ZOMBIE_FRANKEN_MITTE_STEHEN_ANIMATIONSDAUER_EINES_FRAMES * KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_ANZAHL_BILDER,
                Konstanten.ZOMBIE_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES * KonstantenAssets.ZOMBIE_A_LAUFEN_ANZAHL_BILDER,
                0,
                0
        );
    }
    public static float maximalerFloat(float a, float b,float c, float d, float e, float f, float g, float h){
        float a1 = Math.max(a, b);
        float a2 = Math.max(c, d);
        float a3 = Math.max(e, f);
        float a4 = Math.max(g, h);

        float b1 = Math.max(a1, a2);
        float b2 = Math.max(a3, a4);

        return Math.max(b1, b2);
    }

    public static Vector2 positionUntenLinks(Vector2 positionMitteImViereck, Vector2 breiteUndHoeheDesVierecks) {
        return new Vector2(positionMitteImViereck.x-breiteUndHoeheDesVierecks.x/2, positionMitteImViereck.y-breiteUndHoeheDesVierecks.y/2);
    }
    public static boolean berechneKollisionViereckUndPunkt(Vector2 untenLinksPositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        Rectangle rectangle = new Rectangle(untenLinksPositionDesVierecks.x, untenLinksPositionDesVierecks.y, breiteUndHoeheDesVierecks.x, breiteUndHoeheDesVierecks.y);
        return rectangle.contains(positionDesPunktes);
    }
    public static boolean berechneKollisionViereckUndPunktMitMitte(Vector2 mittePositionDesVierecks, Vector2 breiteUndHoeheDesVierecks, Vector2 positionDesPunktes){
        Vector2 untenLinksPositionDesVierecks = positionUntenLinks(mittePositionDesVierecks, breiteUndHoeheDesVierecks);
        return berechneKollisionViereckUndPunkt(untenLinksPositionDesVierecks, breiteUndHoeheDesVierecks, positionDesPunktes);
    }

    public static float secondsSince(long timeNanos) {
        return MathUtils.nanoToSec * (TimeUtils.nanoTime() - timeNanos);
    }
    public static boolean pause(long pauseStartzeit, float pauseDauer){
        if(TimeUtils.nanoTime() - pauseDauer/MathUtils.nanoToSec >= pauseStartzeit){
            return false;
        }else{
            return true;
        }
    }
    public static long timerStartenWennErNochNichtLaueft (long timerStartzeit, float timerDauer) {
        if (pause(timerStartzeit, timerDauer)) {
            return timerStartzeit;
        } else {
            return TimeUtils.nanoTime();
        }
    }
    /*
    public static boolean pauseLiegtZwischen(long pauseStartzeit, float dauerVorPause, float dauerPause, float dauerNachPause){
        return (!pause(pauseStartzeit, dauerVorPause)
                && !pause(pauseStartzeit+(long)(dauerVorPause+dauerPause), dauerNachPause)
                && pause(pauseStartzeit+(long)dauerVorPause, dauerPause));
    }*/

    //Teilweise aus dem Udacity Kurs kopiert
    public static void zeichneTexturRegion(SpriteBatch batch, TextureRegion region, Vector2 position, boolean horizontalSpiegeln) {
        zeichneTexturRegion(batch, region, position.x, position.y, horizontalSpiegeln);
    }
    private static void zeichneTexturRegion(SpriteBatch batch, TextureRegion region, float x, float y, boolean horizontalSpiegeln) {
        batch.draw(
                region.getTexture(),
                x, //horizontalSpiegeln ? x+region.getRegionWidth() : x, kann gelöscht werden aber ich finde den Befehl gut, vielleicht erinnert man sich so daran
                y,
                0,
                0,
                region.getRegionWidth(), //horizontalSpiegeln ? -region.getRegionWidth(): region.getRegionWidth(),
                region.getRegionHeight(),
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                horizontalSpiegeln,
                false);
    }

    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, Vector2 position) {
        drawTextureRegion(batch, region, position.x, position.y);
    }
    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, float groesseModifikator, Vector2 position) {
        drawTextureRegion(batch, region, groesseModifikator, position.x, position.y);
    }

    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, Vector2 position, Vector2 offset) {
        drawTextureRegion(batch, region, position.x - offset.x, position.y - offset.y);
    }
    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, float groesseModifikator, Vector2 position, Vector2 offset) {
        drawTextureRegion(batch, region, groesseModifikator, position.x - offset.x, position.y - offset.y);
    }

    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, float x, float y) {
        batch.draw(
                region.getTexture(),
                x,
                y,
                0,
                0,
                region.getRegionWidth(),
                region.getRegionHeight(),
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false);
    }
    public static void drawTextureRegion(SpriteBatch batch, TextureRegion region, float groesseModifikator, float x, float y) {
        batch.draw(
                region.getTexture(),
                x,
                y* groesseModifikator, //TODO damit die Figur auf dem Boden stehen bleibt. Evtl überarbeiten wenn springen eingeführt wird
                0,
                0,
                (int)(region.getRegionWidth() * groesseModifikator),
                (int)(region.getRegionHeight() * groesseModifikator),
                1,
                1,
                0,
                region.getRegionX(),
                region.getRegionY(),
                region.getRegionWidth(),
                region.getRegionHeight(),
                false,
                false);
    }

    //Integral
    // The example function you provided.
    public double f(double x, double y) {
        return x + y * y;
    }
    /**
     * Finds the volume under the surface described by the function f(x, y) for a <= x <= b, c <= y <= d.
     * Using xSegs number of segments across the x axis and ySegs number of segments across the y axis.
     * @param a The lower bound of x.
     * @param b The upper bound of x.
     * @param c The lower bound of y.
     * @param d The upper bound of y.
     * @param xSegs The number of segments in the x axis.
     * @param ySegs The number of segments in the y axis.
     * @return The volume under f(x, y).
     */
    public double trapezoidRule(double a, double b, double c, double d, int xSegs, int ySegs) {
        double xSegSize = (b - a) / xSegs; // length of an x segment.
        double ySegSize = (d - c) / ySegs; // length of a y segment.
        double volume = 0; // volume under the surface.

        for (int i = 0; i < xSegs; i++) {
            for (int j = 0; j < ySegs; j++) {
                double height = f(a + (xSegSize * i), c + (ySegSize * j));
                height += f(a + (xSegSize * (i + 1)), c + (ySegSize * j));
                height += f(a + (xSegSize * (i + 1)), c + (ySegSize * (j + 1)));
                height += f(a + (xSegSize * i), c + (ySegSize * (j + 1)));
                height /= 4;

                // height is the average value of the corners of the current segment.
                // We can use the average value since a box of this height has the same volume as the original segment shape.

                // Add the volume of the box to the volume.
                volume += xSegSize * ySegSize * height;
            }
        }

        return volume;
    }
}
