package com.evilknight.spiel.werkzeuge;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by LeonPB on 28.10.2017.
 */

public class Konstanten {

    public static final Color HINTERGRUND_FARBE = Color.SKY;
    public static final float WELT_GROESSE = 320; //120
    public static final float WELT_BODEN_HOEHE = 60;//16;
    public static final Vector2 WELT_SCHWERKRAFT = new Vector2(0, -960);//-880);
    public static final float SPIELER_KOYOTE = 3f; //Der spieler kann kurzzeitig wie dieser eine Kojote über die Klippe hinaus in der Luft laufen. Machen viele Spiele und fühlt sich richtig an

    //FigurSpieler
    public static final Vector2 EVIL_KNIGHT_SPAWN_POSITION = new Vector2(300, 100);
    public static final float SPIELER_LEBENSPUNKTE_MAX_ERHOEHEN_FUER_KILL = 10f;
    public static final float SPIELER_SPRINGEN_SELBSTSCHADEN = 10f;
    public static final float SPIELER_SPRINGEN_SELBSTSCHADEN_EXPONENT = 1.3f; //Der Exponent mit dem der Schaden hoch genommen wird
    public static final float RITTER_Si_LEBENSPUNKTE = 150;


    //BildschirmTasten
    public static final float BILDSCHIRMTASTEN_VIEWPORT_GROESSE = 120;
    public static final Vector2 BILDSCHIRMTASTEN_TASTE_BREITE_UND_HOEHE = new Vector2(Assets.instance.bildschirmtastenAssets.links.getRegionWidth(), Assets.instance.bildschirmtastenAssets.links.getRegionHeight());
    public static final Vector2 BILDSCHIRMTASTEN_TASTE_LINKS_LAUFEN_POSITION = new Vector2(20, 20);
    public static final Vector2 BILDSCHIRMTASTEN_TASTE_RECHTS_LAUFEN_POSITION = new Vector2(40, 20);
    public static final Vector2 BILDSCHIRMTASTEN_TASTE_OBEN_LAUFEN_POSITION = new Vector2(30, 35);
    public static final Vector2 BILDSCHIRMTASTEN_TASTE_UNTEN_LAUFEN_POSITION = new Vector2(30, 5);

    //RitterSi
    public static final float RITTER_Si_STEHEN_ANIMATIONSDAUER = 2.4f;
    public static final float RITTER_Si_SCHLAGEN_ANIMATIONSDAUER = 1.05f;
    public static final float RITTER_Si_LAUFEN_ANIMATIONSDAUER = 0.1f;//0.2f
    public static final float RITTER_Si_ZEIT_BIS_IDLE = 3f;

    public static final float EVIL_KNIGHT_BEWEGUNGSGESCHWINDIGKEIT = 100;//30
    public static final float RITTER_SI_SCHLAGEN_SCHADEN = 60;
    public static final float RITTER_Si_SPRUNGKRAFT = 440;//40
    public static final float RITTER_Si_SPRUNG_SELBSTSCHADEN = 20;//40
    public static final float RITTER_Si_GEWICHT = 1;

    //Kamera
    public static final float VERFOLGUNGS_KAMERA_BEWEGUNGSGESCHWINDIGKEIT = 400;
    public static final float VERFOLGUNGS_KAMERA_GUCKRICHTUNG_ERWEITERTES_SICHTFELD = 4;
    public static final float VERFOLGUNGS_KAMERA_VERSCHIEBUNG = 0; //Damit man "Unten"=Den Boden auch sieht

    //Gegner
    public static final float GEGNER_SCHLAGEN_ENTFERNUNG_MODIFIKATOR = 0.80f; //damit der Gegner nicht zuschlägt sobalt die FigurSpieler auch nur einen Pixel in Reichweite ist und dann super einfach ausweichen kann

    //FigurGegnerZombieA
    public static final Vector2 ZOMBIE_A_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(12, 20); //TODO
    public static final Vector2 ZOMBIE_A_SCHLAGEN_HITBOX_BREITE_UND_HOEHE = new Vector2(10, 20); //TODO
    public static final float ZOMBIE_A_SCHLAGEN_VERZOEGERUNG = 0.9f; //Sekunden die vergehen bis der Schaden ausgeführt wird
    public static final float ZOMBIE_A_LAUFEN_ANIMATIONSDAUER = 1.2f;
    public static final float ZOMBIE_A_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES = ZOMBIE_A_LAUFEN_ANIMATIONSDAUER/KonstantenAssets.ZOMBIE_A_LAUFEN_ANZAHL_BILDER;
    public static final float ZOMBIE_A_MITTE_STEHEN_ANIMATIONSDAUER = 1;
    public static final Vector2 ZOMBIE_A_SPAWN_POSITION = new Vector2(-300,WELT_BODEN_HOEHE);

    public static final Vector2 ZOMBIE_A_SICHTWEITE =  new Vector2(40,0);
    public static final float ZOMBIE_A_BEWEGUNGSGESCHWINDIGKEIT = 15;
    public static final float ZOMBIE_A_LEBENSPUNKTE = 100;
    public static final float ZOMBIE_A_SCHLAGEN_SCHADEN = 10;

    //FigurGegnerZombieFranken
    public static final Vector2 ZOMBIE_FRANKEN_SPAWN_POSITION = new Vector2(-200,WELT_BODEN_HOEHE);
    public static final Vector2 ZOMBIE_FRANKEN_FIGUR_HITBOX_BREITE_UND_HOEHE = new Vector2(19, 37);

    public static final Vector2 ZOMBIE_FRANKEN_SCHLAGEN_HITBOX_BREITE_UND_HOEHE = new Vector2(5, 37); //TODO
    public static final float ZOMBIE_FRANKEN_SCHLAGEN_VERZOEGERUNG = 0.9f; //Sekunden die vergehen bis der Schaden ausgeführt wird
    public static final float ZOMBIE_FRANKEN_SCHLAGEN_ANIMATIONSDAUER = 1f;
    public static final float ZOMBIE_FRANKEN_SCHLAGEN_ANIMATIONSDAUER_EINES_FRAMES = ZOMBIE_FRANKEN_SCHLAGEN_ANIMATIONSDAUER/KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_ANZAHL_BILDER;

    public static final float ZOMBIE_FRANKEN_LAUFEN_ANIMATIONDAUER = 1.2f;
    public static final float ZOMBIE_FRANKEN_LAUFEN_ANIMATIONSDAUER_EINES_FRAMES = ZOMBIE_FRANKEN_LAUFEN_ANIMATIONDAUER/KonstantenAssets.ZOMBIE_FRANKEN_LAUFEN_ANZAHL_BILDER;
    public static final float ZOMBIE_FRANKEN_MITTE_STEHEN_ANIMATIONSDAUER = 4;
    public static final float ZOMBIE_FRANKEN_MITTE_STEHEN_ANIMATIONSDAUER_EINES_FRAMES = ZOMBIE_FRANKEN_MITTE_STEHEN_ANIMATIONSDAUER/KonstantenAssets.ZOMBIE_FRANKEN_MITTE_STEHEN_ANZAHL_BILDER;

    public static final Vector2 ZOMBIE_FRANKEN_SICHTWEITE =  new Vector2(40,0);
    public static final float ZOMBIE_FRANKEN_BEWEGUNGSGESCHWINDIGKEIT = 15;
    public static final float ZOMBIE_FRANKEN_LEBENSPUNKTE = 100;
    public static final float ZOMBIE_FRANKEN_SCHLAGEN_SCHADEN = 10;

    //FigurGegnerSkelettA
    public static final Vector2 SKELETT_A_SPAWN_POSITION = new Vector2(100,WELT_BODEN_HOEHE);
    public static final float SKELETT_A_SCHLAGEN_ANIMATIONSDAUER = 1f;
    public static final float SKELETT_A_SCHLAGEN_VERZOEGERUNG = SKELETT_A_SCHLAGEN_ANIMATIONSDAUER* 5 /KonstantenAssets.BAUER_A_SCHLAGEN_ANZAHL_BILDER; //Sekunden die vergehen bis der Schaden ausgeführt wird //Errechnet durch das Bild an dem das Schwert runter kommt/durch alle Bilder und evtl noch etwas modifiziert

    public static final float SKELETT_A_LAUFEN_ANIMATIONDAUER = 1f;
    public static final float SKELETT_A_STEHEN_ANIMATIONSDAUER = 0.6f;
    public static final float SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONDAUER = 1f;
    public static final float SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONDAUER = 0.1f;
    public static final float SKELETT_A_BLOCKEN_DAUER = SKELETT_A_BLOCKEN_BEGINNEN_ANIMATIONDAUER + SKELETT_A_BLOCKEN_BEENDEN_ANIMATIONDAUER + 2f;

    public static final Vector2 SKELETT_A_SICHTWEITE =  new Vector2(350, 0);
    public static final float SKELETT_A_BEWEGUNGSGESCHWINDIGKEIT = 70;
    public static final float SKELETT_A_SPRUNGKRAFT = 100;
    public static final float SKELETT_A_LEBENSPUNKTE = 100;
    public static final float SKELETT_A_SCHLAGEN_SCHADEN = 40;

    ////FigurGegnerBauerA
    //Müssen ausgefüllt werden
    public static final Vector2 BAUER_A_SICHTWEITE = new Vector2(280,0);
    public static final float BAUER_A_BEWEGUNGSGESCHWINDIGKEIT = 50;
    public static final float BAUER_A_SPRUNGKRAFT = 100;
    public static final float BAUER_A_LEBENSPUNKTE = 100;
    public static final float BAUER_A_SCHLAGEN_SCHADEN = 30;
    //
    public static final float BAUER_A_SCHLAGEN_VERZOEGERUNG = 1f; //Sekunden die vergehen bis der Schaden ausgeführt wird
    public static final float BAUER_A_SCHLAGEN_ANIMATIONSDAUER = 1.5f;
    public static final float BAUER_A_STEHEN_ANIMATIONSDAUER = 0.8f;
    public static final float BAUER_A_LAUFEN_ANIMATIONDAUER = 1f;
    public static final float BAUER_A_STUERMEN_ANIMATIONDAUER = 0.8f;
    public static final float BAUER_A_STERBEN_ANIMATIONDAUER = 1f;
    //Optional
    public static final Vector2 BAUER_A_SPAWN_POSITION = new Vector2(100,WELT_BODEN_HOEHE);
    //
    public static final boolean BAUER_A_KANN_STUERMEN = true;
    public static final float BAUER_A_STUERMEN_MINDESTENTFERNUNG = 200f;
    public static final float BAUER_A_STUERMEN_REICHWEITE = BAUER_A_SICHTWEITE.x *1.5f;
    public static final float BAUER_A_STUERMEN_BEWEGUNGSGESCHWINDIGKEIT = 110f;
    public static final float BAUER_A_STUERMEN_SCHADEN = BAUER_A_SCHLAGEN_SCHADEN *4; //Schaden pro Sekunde
    public static final long BAUER_A_STUERMEN_DAUER = (long)(BAUER_A_STUERMEN_REICHWEITE / BAUER_A_STUERMEN_BEWEGUNGSGESCHWINDIGKEIT);

    ////FigurGegnerBogenschuetzeA
    //Müssen ausgefüllt werden
    public static final Vector2 BOGENSCHUETZE_A_SICHTWEITE = new Vector2(500, KonstantenAssets.BOGENSCHUETZE_A_FIGUR_HITBOX_BREITE_UND_HOEHE.y/2 + 30);
    public static final float BOGENSCHUETZE_A_BEWEGUNGSGESCHWINDIGKEIT = 120f;
    public static final float BOGENSCHUETZE_A_SPRUNGKRAFT = 100;
    public static final float BOGENSCHUETZE_A_LEBENSPUNKTE = 50;
    public static final float BOGENSCHUETZE_A_SCHLAGEN_SCHADEN = 40;
    //
    public static final boolean BOGENSCHUETZE_A_KANN_SCHIESSEN = true;
    public static final float BOGENSCHUETZE_A_SCHIESSEN_MINDESTENTFERNUNG = 250f;
    public static final float BOGENSCHUETZE_A_SCHIESSEN_REICHWEITE = 300f;
    public static final float BOGENSCHUETZE_A_SCHIESSEN_ANIMATIONSDAUER = 2.5f;//0.05f;//
    public static final float BOGENSCHUETZE_A_STEHEN_ANIMATIONSDAUER = 0.6f;
    public static final float BOGENSCHUETZE_A_LAUFEN_ANIMATIONDAUER = 1f;
    public static final float BOGENSCHUETZE_A_STUERMEN_ANIMATIONDAUER = 0.8f;
    public static final float BOGENSCHUETZE_A_STERBEN_ANIMATIONDAUER = 1f;
    //Pfeil
    public static final float PFEIL_BEWEGUNGSGESCHWINDIGKEIT = 280f;
    public static final float PFEIL_GEWICHT = 0.15f;
    public static final float PFEIL_SCHADEN = 40f;

}
