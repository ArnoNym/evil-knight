<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tiles-grassAtsxBLA" tilewidth="32" tileheight="32" tilecount="1600" columns="40">
 <image source="level_1_tiletextures_1.png" trans="ff00ff" width="1280" height="1280"/>
 <tile id="21">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="115">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="116">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
  </properties>
 </tile>
 <tile id="176">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="177">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="178">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="200">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
  </properties>
 </tile>
 <tile id="201">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="202">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
  </properties>
 </tile>
 <tile id="240">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="241">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="242">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="280">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="281">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="282">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="320">
  <animation>
   <frame tileid="320" duration="500"/>
   <frame tileid="360" duration="500"/>
   <frame tileid="400" duration="500"/>
   <frame tileid="360" duration="500"/>
  </animation>
 </tile>
 <tile id="321">
  <animation>
   <frame tileid="321" duration="500"/>
   <frame tileid="361" duration="500"/>
   <frame tileid="401" duration="500"/>
   <frame tileid="361" duration="500"/>
  </animation>
 </tile>
 <tile id="322">
  <animation>
   <frame tileid="322" duration="500"/>
   <frame tileid="362" duration="500"/>
   <frame tileid="402" duration="500"/>
   <frame tileid="362" duration="500"/>
  </animation>
 </tile>
 <tile id="323">
  <animation>
   <frame tileid="323" duration="500"/>
   <frame tileid="363" duration="500"/>
   <frame tileid="403" duration="500"/>
   <frame tileid="363" duration="500"/>
  </animation>
 </tile>
 <tile id="324">
  <animation>
   <frame tileid="324" duration="500"/>
   <frame tileid="364" duration="500"/>
   <frame tileid="404" duration="500"/>
   <frame tileid="364" duration="500"/>
  </animation>
 </tile>
 <tile id="327">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="342">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="343">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="344">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="345">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="346">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="352">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="353">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="354">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="355">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="356">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="357">
  <properties>
   <property name="block_oben" value=""/>
  </properties>
 </tile>
 <tile id="440" probability="0.25">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
 <tile id="441">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
 <tile id="442">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
 <tile id="480">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
 <tile id="481">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
 <tile id="482">
  <properties>
   <property name="block_links" value=""/>
   <property name="block_oben" value=""/>
   <property name="block_rechts" value=""/>
   <property name="block_unten" value=""/>
  </properties>
 </tile>
</tileset>
