<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tiles-spawnen" tilewidth="32" tileheight="32" tilecount="400" columns="20">
 <image source="level_1_tiletexturesSpawnen_1.png" trans="ff00ff" width="640" height="640"/>
 <tile id="41">
  <properties>
   <property name="bauerA" value=""/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="skelettA" value=""/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="bogenschuetzeA" value=""/>
  </properties>
 </tile>
</tileset>
