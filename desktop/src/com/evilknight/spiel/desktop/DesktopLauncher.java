package com.evilknight.spiel.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.evilknight.spiel.EvilKnightSpiel;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		//Ab hier --- Selbst dazu geschrieben
		config.title = "20180310 Alpha";
		//config.width = 1600;
		//config.height = 900;
		//config.width = Gdx.graphics.getDesktopDisplayMode().width;
		//config.height = Gdx.graphics.getDesktopDisplayMode().height;
		//config.vSyncEnabled = true;
		//Bis hier --- Selbst dazu geschrieben
		new LwjglApplication(new EvilKnightSpiel(), config);
	}
}
